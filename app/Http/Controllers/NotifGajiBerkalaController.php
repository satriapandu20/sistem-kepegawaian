<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotifGajiBerkala;
use App\Pegawai;
use App\RiwayatGajiBerkala;


class NotifGajiBerkalaController extends Controller
{
    //
    function index()
    {
    	$y=date('Y');
        $m=date('m');
        
        $yTerakhir_Naik_Gaji;


        if($m>9)
        {
            if($m==10){
                $m=00;
                $m=$m+1;
            }
            else if($m==11)
            { 
                $m=00;
                $m=$m+2;
            }
            else{

                $m=00;
                $m=$m+3;

            }
            
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $y=$y+1;
            $yTerakhir_Naik_Gaji=$y-1;
        }
        else
        {
            $m=$m+3;
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $yTerakhir_Naik_Gaji=$y-1;
            
        }



        $lastKGB=$yTerakhir_Naik_Gaji.'-'.$m;
        $currentKGB=$y.'-'.$m;

        $ngb=NotifGajiBerkala::join('tb_pegawai','tb_history_notif_gaji_berkala.id_pegawai','=','tb_pegawai.id_pegawai')
        	->join('tb_riwayat_gaji_berkala','tb_history_notif_gaji_berkala.id_riwayat_gaji_berkala','=','tb_riwayat_gaji_berkala.id_riwayat_gaji_berkala')
        	->where('waktu_gaji_berkala','like','%'.$currentKGB.'%')->get();

        if($m=="01")
                    {
                        $m='Januari';
                    }
                    else if($m=="02")
                    {
                        $m='Februari';
                    }
                    else if($m=="03")
                    {
                        $m='Maret';
                    }
                    else if($m=="04")
                    {
                        $m='April';
                    }
                    else if($m=="05")
                    {
                        $m='Mei';
                    }
                    else if($m=="06")
                    {
                        $m='Juni';
                    }
                    else if($m=="07")
                    {
                        $m='Juli';
                    }
                    else if($m=="08")
                    {
                        $m='Agustus';
                    }
                    else if($m=="09")
                    {
                        $m='September';
                    }
                    else if($m=="10")
                    {
                        $m='Oktober';
                    }
                    else if($m=="11")
                    {
                        $m='November';
                    }
                    else if($m=="12")
                    {
                        $m='Desember';
                    }


        return view('notify.notif-gaji',compact('ngb','y','m'));

    }
}
