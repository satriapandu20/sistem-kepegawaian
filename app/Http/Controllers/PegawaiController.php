<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pegawai;
use App\RiwayatGajiBerkala;
use App\Users;
use App\RiwayatPendidikan;
use App\NotifPensiun;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;
use PDF;
use Illuminate\Support\Facades\Input;

use App\Mail\MailUser;
use Illuminate\Support\Facades\Mail;

class PegawaiController extends Controller
{
    public function dashboard(){

    if(!Session::get('login'))
    {
        return view('all/login');
    }

        $pg=Pegawai::count();
        //===================
        $y=date('Y');
        $m=date('m');
        

        $yTerakhir_Naik_Gaji;


        if($m>9)
        {
            if($m==10){
                $m=00;
                $m=$m+1;
            }
            else if($m==11)
            { 
                $m=00;
                $m=$m+2;
            }
            else{

                $m=00;
                $m=$m+3;

            }
            
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $y=$y+1;
            $yTerakhir_Naik_Gaji=$y-1;
        }
        else
        {
            $m=$m+3;
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $yTerakhir_Naik_Gaji=$y-1;
            
        }


        $lastKGB=$yTerakhir_Naik_Gaji.'-'.$m;


        $rgb=RiwayatGajiBerkala::join('tb_pegawai','tb_riwayat_gaji_berkala.id_pegawai','=','tb_pegawai.id_pegawai')
            ->where('TMT_KGB','like','%'.$lastKGB.'%')->count();

        //============================================
        $tahun=date('Y');
        $tahun=$tahun+1;
     
        $pensiun=Pegawai::where('tahun_pensiun',$tahun)->count();


        return view('all/dashboard',compact('pg','rgb','pensiun'));
    }

    public function UserDashboard($id){

        if(!Session::get('user'))
        {
            return view('all/login')->with('warning','Silahkan login');
        }

        if($id==Session::get('user')){
            $pg=Pegawai::find($id);
            $kgb=RiwayatGajiBerkala::where('id_pegawai',$id)->orderBy('TMT_KGB','desc')->first();
            if($kgb!=""||$kgb!=null){
            $kgb=$kgb->TMT_KGB;
            $year=substr($kgb,0,4)+1;
            $kgb=$year.substr($kgb,4,6);
            }else{
            $kgb='Belum ada riwayat gaji berkala';
            }

            
            $pendidikan=RiwayatPendidikan::where('id_pegawai',$id)->count();


            if($pendidikan==0){
                return view('user/dashboard')->with('pg',$pg)->with('kgb',$kgb)->with('pendidikan',' *Mohon lengkapi data pendidikan anda !');    
            }else{
                return view('user/dashboard',compact('pg','kgb'))->with('pendidikan','');
            }
        }else{
            abort(404);
        }

        
    }
    //
    public function index()
    {
        if(!Session::get('admin'))
        {
            return abort(404);
        }else{
        
        $pg= Pegawai::where('status_keaktifan','Aktif')->get();
    	return view('pegawai/home',compact('pg'));
        }
    }

    public function search_index(){
        if((!Session::get('login')) && (!Session::get('admin')))
        {
            return view('all/login');
        }
        
        $query = Input::get ( 'query' );

        if($query==""){
        $pg= Pegawai::all();
        }else{

        
        $pg= Pegawai::where('nama','like','%'.$query.'%')->where('status_keaktifan','Aktif')->get();
        }
        return view('pegawai/home',compact('pg'));
    }


    // public function loadIndex(Request $req)
    // {
    //     if($request->ajax())
    //     {
            
    //         $output = '';
    //         $query = $request->get('query');

    //           if($query != '')
    //           {
    //             $data = Pegawai::where('nama','like','%'.$query.'%')->get();
                 
    //           }
    //           else
    //           {
    //             $data = Pegawai::where('status_keaktifan','Aktif')->get();
    //           }

    //           $total_row = $data->count();
    //           if($total_row > 0)
    //           {
    //            foreach($data as $row)
    //            {
    //             $output .= '

    //             <tr>
    //                 <td></td>
    //                 <td class="text-uppercase">'.$row->NIK.'</td>
    //                 <td class="text-uppercase">'.$row->nama.'</td>
    //                 <td align="center">
    //                 <a href="{{ url("pegawai/"'.$row->id_pegawai.'"><button type="button" class="btn btn-info"><i class="fa fa-edit"></i></button></a>
    //                               <a href="#"><button type="button" name="button" class="btn btn-danger deleteUser" data-userid="'.$row->id_pegawai.'"><i class="fa fa-trash"></i></button></a>

    //                 </td>

    //             </tr>
    //             ';
    //            }
    //           }else{
    //            $output = '
    //            <tr>
    //             <td align="center" colspan="4">No Data Found</td>
    //            </tr>
    //            ';
    //           }
    //     $output = '
    //            <tr>
    //             <td align="center" colspan="4">No Data Found</td>
    //            </tr>
    //            ';

    //       $data = array(
    //        'table_data'  => $output
    //       );

    //         echo json_encode($data);
    //     }
    // }
    public function indexNonAktif()
    {
        if((!Session::get('login')) && (!Session::get('admin')))
        {
            return view('all/login');
        }
        
        $pg= Pegawai::where('status_keaktifan','Tidak Aktif')->get();
        return view('pegawai/non-aktif',['pgs'=>$pg]);
    }

    public function create()
    {
    	return view('pegawai/create');
    }
    public function store(Request $req)
    {

        $req->validate([
            'nama'=>'required',
            'nik'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'bulan_lahir'=>'required',
            'tahun_lahir'=>'required',
            'nama_ibu_kandung'=>'required',
            'alamat'=>'required',
            'rt'=>'required|max:2',
            'rw'=>'required|max:2',
            'kelurahan'=>'required',
            'kecamatan'=>'required',
            'kabupaten'=>'required',
            'kode_pos'=>'required',
            'agama'=>'required',
            'status_perkawinan'=>'required',

            'jenis_gtk'=>'required',
            'status_kepegawaian'=>'required',
            'sumber_gaji'=>'required',
            'masa_kerja'=>'required',
            'tahun_pensiun'=>'required',
            'nomor_telepon'=>'required',
            'email'=>'required',
            'foto'=>'required'
        ]);

    	$pg= new Pegawai;
    	$pg->id_pegawai=$this->countID();
    	$pg->nama=$req->nama;
    	$pg->nik=$req->nik;
    	$pg->jenis_kelamin=$req->jenis_kelamin;
    	$pg->tempat_lahir=$req->tempat_lahir;
    	$pg->tanggal_lahir=$req->tahun_lahir."-".$req->bulan_lahir."-".$req->tanggal_lahir;
    	$pg->nama_ibu_kandung=$req->nama_ibu_kandung;
    	$pg->alamat=$req->alamat;
        $pg->rt=$req->rt;
        $pg->rw=$req->rw;
        $pg->kelurahan=$req->kelurahan;
        $pg->kecamatan=$req->kecamatan;
        $pg->kabupaten=$req->kabupaten;
        $pg->kode_pos=$req->kode_pos;
    	$pg->agama=$req->agama;
    	$pg->status_perkawinan=$req->status_perkawinan;
    	$pg->nama_pasangan=$req->nama_pasangan;
    	$pg->pekerjaan_pasangan=$req->pekerjaan_pasangan;
    	$pg->npwp=$req->npwp;
    	$pg->nama_wajib_pajak=$req->nama_wajib_pajak;
    	$pg->jenis_gtk=$req->jenis_gtk;
    	$pg->nuptk=$req->nuptk;
    	$pg->status_kepegawaian=$req->status_kepegawaian;
    	$pg->nip=$req->nip;
    	$pg->sk_cpns=$req->sk_cpns;

        if($req->tahun_tmt_cpns!="" && $req->bulan_tmt_cpns != "" && $req->tanggal_tmt_cpns!=""){

    	   $pg->tmt_cpns=$req->tahun_tmt_cpns."-".$req->bulan_tmt_cpns."-".$req->tanggal_tmt_cpns;

        }

        if($req->tahun_tmt_sk_pengangkatan != "" && $req->bulan_tmt_sk_pengangkatan !="" && $req->tanggal_tmt_sk_pengangkatan != ""){

    	   $pg->tmt_sk_pengangkatan=$req->tahun_tmt_sk_pengangkatan."-".$req->bulan_tmt_sk_pengangkatan."-".$req->tanggal_tmt_sk_pengangkatan;
           
        }

    	$pg->sumber_gaji=$req->sumber_gaji;
    	// $pg->pangkat_golongan=$req->pangkat_golongan;
    	// $pg->tmt_pangkat_golongan=$req->tahun_tmt_pangkat_golongan."-".$req->bulan_tmt_pangkat_golongan."-".$req->tanggal_tmt_pangkat_golongan;
    	$pg->masa_kerja=$req->masa_kerja;
    	// $pg->sk_pengangkatan=$req->sk_pengangkatan;
    	// $pg->tmt_pengangkatan=$req->tahun_tmt_pengangkatan."-".$req->bulan_tmt_pengangkatan."-".$req->tanggal_tmt_pengangkatan;
    	// $pg->lembaga_pengangkatan=$req->lembaga_pengangkatan;
    	$pg->kartu_pegawai=$req->kartu_pegawai;
    	$pg->kartu_pasangan=$req->kartu_pasangan;
    	
    	
    	$pg->email=$req->email;
        $pg->nomor_telepon=$req->nomor_telepon;
    	$pg->tahun_pensiun=$req->tahun_pensiun;//

        $req->validate([

            'foto' => 'required|image|mimes:jpeg,png,jpg|max:2048'

            ]);
        $image=$req->file('foto');
        $new_name=rand() .'.'.$image->getClientOriginalExtension();
        $image->move(public_path('images'),$new_name);

        $pg->foto=$new_name;

    	$pg->save();

        $user= new Users();
        $user->id_pegawai=$pg->id_pegawai;
        $user->username=$pg->nik;

        $pass=str_random(6);
        $password=bcrypt($pass);

        $user->password=$password;
        $user->save();

        $pensi=new NotifPensiun;
        $pensi->id_pegawai=$pg->id_pegawai;
        $pensi->save();
        
        // return back()->with('success','Berhasil Upload')->with('path',$new_name);



        return $this->SendUser($pg->id_pegawai,$user->username,$pass);



    }
    
    public function countID() {
   
        $kd= Pegawai::select()->max('id_pegawai');
       
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,3,7);
                $newkode = $sub + 1;
                $ID = "PG-".sprintf('%04s',$newkode);
            }
        }else
        {
            $ID="PG-0001";

        }
        return $ID;
    }
    //--------------------------
    public function edit($id){
        if(!Session::get('login')){
            return redirect('login')->with('error','Silahkan login..');
        }else{
            if(Session::get('user')!=""||Session::get('user')!=null)
            {
                $user=Session::get('user');
                if($id==$user){
                    $pg=Pegawai::find($user);


                    $d_tgl_lahir=substr($pg->tanggal_lahir,8);
                    $m_tgl_lahir=substr($pg->tanggal_lahir,5,2);
                    $y_tgl_lahir=substr($pg->tanggal_lahir,0,4);
                    
                    $tanggal_lahir=array(
                        "d"=>$d_tgl_lahir,
                        "m"=>$m_tgl_lahir,
                        "y"=>$y_tgl_lahir,
                    );

                    $d_TMT_CPNS=substr($pg->TMT_CPNS,8);
                    $m_TMT_CPNS=substr($pg->TMT_CPNS,5,2);
                    $y_TMT_CPNS=substr($pg->TMT_CPNS,0,4);
                    
                    $tmt_cpns=array(
                        "d"=>$d_TMT_CPNS,
                        "m"=>$m_TMT_CPNS,
                        "y"=>$y_TMT_CPNS,
                    );

                    $d_TMT_SK=substr($pg->TMT_SK_pengangkatan,8);
                    $m_TMT_SK=substr($pg->TMT_SK_pengangkatan,5,2);
                    $y_TMT_SK=substr($pg->TMT_SK_pengangkatan,0,4);
                    
                    $tmt_sk_pengangkatan=array(
                        "d"=>$d_TMT_SK,
                        "m"=>$m_TMT_SK,
                        "y"=>$y_TMT_SK,
                    );

                    

                    return view('pegawai/edit',compact('pg','tanggal_lahir','tmt_cpns','tmt_sk_pengangkatan'));
                }else{
                    abort(404);
                }
            }
            else if(Session::get('admin')!=""||Session::get('admin')!=null)
            {
                    $pg=Pegawai::find($id);


                    $d_tgl_lahir=substr($pg->tanggal_lahir,8);
                    $m_tgl_lahir=substr($pg->tanggal_lahir,5,2);
                    $y_tgl_lahir=substr($pg->tanggal_lahir,0,4);
                    
                    $tanggal_lahir=array(
                        "d"=>$d_tgl_lahir,
                        "m"=>$m_tgl_lahir,
                        "y"=>$y_tgl_lahir,
                    );

                    $d_TMT_CPNS=substr($pg->TMT_CPNS,8);
                    $m_TMT_CPNS=substr($pg->TMT_CPNS,5,2);
                    $y_TMT_CPNS=substr($pg->TMT_CPNS,0,4);
                    
                    $tmt_cpns=array(
                        "d"=>$d_TMT_CPNS,
                        "m"=>$m_TMT_CPNS,
                        "y"=>$y_TMT_CPNS,
                    );

                    $d_TMT_SK=substr($pg->TMT_SK_pengangkatan,8);
                    $m_TMT_SK=substr($pg->TMT_SK_pengangkatan,5,2);
                    $y_TMT_SK=substr($pg->TMT_SK_pengangkatan,0,4);
                    
                    $tmt_sk_pengangkatan=array(
                        "d"=>$d_TMT_SK,
                        "m"=>$m_TMT_SK,
                        "y"=>$y_TMT_SK,
                    );

                    

                    return view('pegawai/edit',compact('pg','tanggal_lahir','tmt_cpns','tmt_sk_pengangkatan'));
            }
        
        }
    }

    public function update(Request $req,$id){
        $pg=Pegawai::where('id_pegawai',$id)->first();
        $pg->nama=$req->nama;
        $pg->nik=$req->nik;
        $pg->jenis_kelamin=$req->jenis_kelamin;
        $pg->tempat_lahir=$req->tempat_lahir;
        $pg->tanggal_lahir=$req->tahun_lahir."-".$req->bulan_lahir."-".$req->tanggal_lahir;
        $pg->nama_ibu_kandung=$req->nama_ibu_kandung;
        $pg->alamat=$req->alamat;
        $pg->agama=$req->agama;
        $pg->status_perkawinan=$req->status_perkawinan;
        $pg->nama_pasangan=$req->nama_pasangan;
        $pg->pekerjaan_pasangan=$req->pekerjaan_pasangan;
        $pg->npwp=$req->npwp;
        $pg->nama_wajib_pajak=$req->nama_wajib_pajak;
        $pg->jenis_gtk=$req->jenis_gtk;
        $pg->nuptk=$req->nuptk;
        $pg->status_kepegawaian=$req->status_kepegawaian;
        $pg->nip=$req->nip;
        $pg->sk_cpns=$req->sk_cpns;
        $pg->tmt_cpns=$req->tmt_cpns;
        $pg->tmt_sk_pengangkatan=$req->tmt_sk_pengangkatan;
        $pg->sumber_gaji=$req->sumber_gaji;
        // $pg->pangkat_golongan=$req->pangkat_golongan;
        // $pg->tmt_pangkat_golongan=$req->tmt_pangkat_golongan;
        $pg->masa_kerja=$req->masa_kerja;
        // $pg->sk_pengangkatan=$req->sk_pengangkatan;
        // $pg->tmt_pengangkatan=$req->tmt_sk_pengangkatan;
        // $pg->lembaga_pengangkatan=$req->lembaga_pengangkatan;
        $pg->kartu_pegawai=$req->kartu_pegawai;
        $pg->kartu_pasangan=$req->kartu_pasangan;
        // $pg->kompetensi=$req->kompetensi;
        // $pg->pendidikan_terakhir=$req->pendidikan_terakhir;
        $pg->email=$req->email;
        $pg->nomor_telepon=$req->nomor_telepon;
        $pg->tahun_pensiun=$req->tahun_pensiun;

        if($req->tahun_tmt_cpns!="" && $req->bulan_tmt_cpns != "" && $req->tanggal_tmt_cpns!=""){

           $pg->tmt_cpns=$req->tahun_tmt_cpns."-".$req->bulan_tmt_cpns."-".$req->tanggal_tmt_cpns;

        }

        if($req->tahun_tmt_sk_pengangkatan != "" && $req->bulan_tmt_sk_pengangkatan !="" && $req->tanggal_tmt_sk_pengangkatan != ""){

           $pg->tmt_sk_pengangkatan=$req->tahun_tmt_sk_pengangkatan."-".$req->bulan_tmt_sk_pengangkatan."-".$req->tanggal_tmt_sk_pengangkatan;
           
        }

        if($req->foto!="")
        {
            $req->validate([

                'foto' => 'image|mimes:jpeg,png,jpg|max:2048'

                ]);
            $image=$req->file('foto');
            $new_name=rand() .'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images'),$new_name);

            $pg->foto=$new_name;
        }


        $pg->save();

        return back()->with('success','Berhasil Mengubah Data');  

    }

    public function delete($id){
        $pg=Pegawai::find($id);
        $pg->delete();

        return back()->with('success','Berhasil Menghapus Data');
    }

    public function NonAktif(Request $req)
    {
        $pg=Pegawai::where('id_pegawai',$req->id_pegawai)->first();
        
        $pg->status_keaktifan='Tidak Aktif';


        $pg->save();

        return back()->with('success','Berhasil Menonaktifkan Pegawai');
    }

    public function Aktif(Request $req)
    {
        $pg=Pegawai::where('id_pegawai',$req->id_pegawai)->first();
        
        $pg->status_keaktifan='Aktif';


        $pg->save();

        return back()->with('success','Berhasil Mengaktifkan Pegawai');
    }



    public function all($id){
        $pg=Pegawai::find($id);
        
        $pdf = PDF::loadView('all/print-user', ['pg'=>$pg])->setPaper('a4');
        return $pdf->stream('PROFIL GURU - '.strtoupper($pg->nama).'.pdf');
        // return view('all/print-user',['pg'=>$pg]);
    }

    public function pensiun(){
        $tahun=date('Y');
        $tahun=$tahun+1;
     
        $pg=Pegawai::where('tahun_pensiun',$tahun)->get();

        
        return view('all/pensiun',compact('tahun','pg'));
    }

    public function naik_gaji(){
        //datenow
        $y=date('Y');
        $m=date('m');
        

        $yTerakhir_Naik_Gaji;


        if($m>9)
        {
            if($m==10){
                $m=00;
                $m=$m+1;
            }
            else if($m==11)
            { 
                $m=00;
                $m=$m+2;
            }
            else{

                $m=00;
                $m=$m+3;

            }
            
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $y=$y+1;
            $yTerakhir_Naik_Gaji=$y-1;
        }
        else
        {
            $m=$m+3;
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $yTerakhir_Naik_Gaji=$y-1;
            
        }


        $lastKGB=$yTerakhir_Naik_Gaji.'-'.$m;

                    if($m=="01")
                    {
                        $m='Januari';
                    }
                    else if($m=="02")
                    {
                        $m='Februari';
                    }
                    else if($m=="03")
                    {
                        $m='Maret';
                    }
                    else if($m=="04")
                    {
                        $m='April';
                    }
                    else if($m=="05")
                    {
                        $m='Mei';
                    }
                    else if($m=="06")
                    {
                        $m='Juni';
                    }
                    else if($m=="07")
                    {
                        $m='Juli';
                    }
                    else if($m=="08")
                    {
                        $m='Agustus';
                    }
                    else if($m=="09")
                    {
                        $m='September';
                    }
                    else if($m=="10")
                    {
                        $m='Oktober';
                    }
                    else if($m=="11")
                    {
                        $m='November';
                    }
                    else if($m=="12")
                    {
                        $m='Desember';
                    }

        
        $rgb=RiwayatGajiBerkala::join('tb_pegawai','tb_riwayat_gaji_berkala.id_pegawai','=','tb_pegawai.id_pegawai')
            ->where('TMT_KGB','like','%'.$lastKGB.'%')->get();

        
        return view('all/riwayat-gaji',compact('rgb','y','m'));
    }

    public function SendUser($id,$username,$pass)//Kirim password user to Email
    {

        $pg=Pegawai::find($id);
        

        $objMail = new \stdClass();
        $objMail->demo_one = $username;
        $objMail->demo_two = $pass;
        $objMail->sender = 'SMK NEGERI 1 PURWOKERTO';
        $objMail->receiver = strtoupper($pg->nama);
 
        Mail::to($pg->email)->send(new MailUser($objMail));

        return redirect('pegawai')->with('success','Berhasil Menambah Data');

    }

    

}
