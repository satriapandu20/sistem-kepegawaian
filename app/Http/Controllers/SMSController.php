<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;
use App\Http\Controllers\Controller;
use App\Pegawai;

class SMSController extends Controller
{
    //
    public function sendSMSpensiun($id){

    	$pg=Pegawai::find($id);


        Nexmo::message()->send([
            'to'=>'62'.$pg->nomor_telepon,
            'from'=>'SMKN 1 PURWOKERTO',
            'text' => 'Anda akan pensiun pada tahun '.$pg->tahun_pensiun.', segera siapkan berkas-berkas anda. Terimakasih'
        ]);

        return back()->with('success','Berhasil mengirim SMS Notifikasi');
    }

    public function sendSMSgaji($id){
    	$pg=Pegawai::find($id);
    	$y=date('Y');
        $m=date('m');

        


        if($m>9)
        {
            if($m==10){
                $m=00;
                $m=$m+1;
            }
            else if($m==11)
            { 
                $m=00;
                $m=$m+2;
            }
            else{

                $m=00;
                $m=$m+3;

            }
            
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $y=$y+1;
            
        }
        else
        {
            $m=$m+3;
            if(strlen($m)==1){
                $m='0'.$m;
            }
            
            
        }

	        		if($m=="01")
					{
						$m='Januari';
					}
					else if($m=="02")
					{
						$m='Februari';
					}
					else if($m=="03")
					{
						$m='Maret';
					}
					else if($m=="04")
					{
						$m='April';
					}
					else if($m=="05")
					{
						$m='Mei';
					}
					else if($m=="06")
					{
						$m='Juni';
					}
					else if($m=="07")
					{
						$m='Juli';
					}
					else if($m=="08")
					{
						$m='Agustus';
					}
					else if($m=="09")
					{
						$m='September';
					}
					else if($m=="10")
					{
						$m='Oktober';
					}
					else if($m=="11")
					{
						$m='November';
					}
					else if($m=="12")
					{
						$m='Desember';
					}

        Nexmo::message()->send([
            'to'=>'62'.$pg->nomor_telepon,
            'from'=>'SMECONE',
            'text' => 'Pemberitahuan kepada '.$pg->nama.',
Anda akan naik gaji pada bulan '.$m.' tahun '.$y.', segera persiapkan berkas-berkas anda.
Terimakasih'
        ]);

        return back()->with('success','Berhasil mengirim SMS Notifikasi');
    }
}
