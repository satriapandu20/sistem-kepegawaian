<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KaryaTulis;
use App\Pegawai;
use Illuminate\Support\Facades\Session;

class KaryaController extends Controller
{
    //
    function index($id){
        if(!Session::get('login')){
            return redirect('login')->with('error','Silahkan login..');
        }else{
            if(Session::get('user')!=""||Session::get('user')!=null)
            {
                $user=Session::get('user');
                if($id==$user){
            	$pg=Pegawai::find($id);
            	$kt=KaryaTulis::where('id_pegawai',$id)->get();

            	return view('karya/home',['karya'=>$kt],['pg'=>$pg]);
                }else{
                    abort(404);
                }
            }else if(Session::get('admin')!=""||Session::get('admin')!=null){
                $pg=Pegawai::find($id);
                $kt=KaryaTulis::where('id_pegawai',$id)->get();

                return view('karya/home',['karya'=>$kt],['pg'=>$pg]);
            }
        }

    }

    function create($idp){
    	$pg=Pegawai::find($idp);
    	return view('karya/create',['pg'=>$pg]);
    }
    function store(Request $req, $idp){

        $req->validate([
            'judul'=>'required',
            'tahun'=>'required',
            'publikasi'=>'required',
            


        ]);
    	$kt=new KaryaTulis;
    	$kt->id_karya=$this->countID();
    	$kt->id_pegawai=$idp;

    	$kt->judul=$req->judul;
    	$kt->tahun=$req->tahun;
    	$kt->publikasi=$req->publikasi;
    	$kt->keterangan=$req->keterangan;

    	$kt->save();

    	return redirect('pegawai/'.$idp.'/karya')->with('success','Berhasil menambah karya');

    }

    function edit($idp,$idk){
    	$pg=Pegawai::find($idp);
    	
    	$kt=KaryaTulis::find($idk);
    	
    	

    	return view('karya/edit',['kt'=>$kt],['pg'=>$pg]);
    }

    function update($idp,$idk, Request $req){
    	$kt=KaryaTulis::where('id_karya',$idk)->where('id_pegawai',$idp)->first();

    	$kt->judul=$req->judul;
    	$kt->tahun=$req->tahun;
    	$kt->publikasi=$req->publikasi;
    	$kt->keterangan=$req->keterangan;

    	$kt->save();

    	return redirect()->back()->with('success','Berhasil mengubah data');

    }

    function delete(Request $req){

    	$kt=KaryaTulis::where('id_karya',$req->id_karya)->where('id_pegawai',$req->id_pegawai)->first();
    	$kt->delete();

    	return back()->with('success','Berhasil menghapus data');

    }

//----------------------------------

    public function countID() {
   
        $kd= KaryaTulis::select()->max('id_karya');
       	
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,4,8);
                $newkode = $sub + 1;
                $ID = "KRY-".sprintf('%05s',$newkode);
            }
        }else
        {
            $ID="KRY-00001";

        }
        return $ID;
    }
}
