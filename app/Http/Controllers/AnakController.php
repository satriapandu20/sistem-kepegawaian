<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anak;
use App\Pegawai;
use DB;

use Illuminate\Support\Facades\Session;


class AnakController extends Controller
{
    //
    public function index($id){

        if(!Session::get('login')){
            return redirect('login')->with('error','Silahkan login..');
        }else{
            if(Session::get('user')!=""||Session::get('user')!=null)
            {
                $user=Session::get('user');
                if($id==$user){

            	$pg=Pegawai::find($id);
            	$anak=Anak::where('id_pegawai',$id)->get();

            	return view('anak/home',['ank'=>$anak], ['pg'=>$pg]);
                }else{
                    abort(404);
                }
            }else if(Session::get('admin')!=""||Session::get('admin')!=null){

                $user=Session::get('user');

                $pg=Pegawai::find($id);
                $anak=Anak::where('id_pegawai',$id)->get();

                return view('anak/home',['ank'=>$anak], ['pg'=>$pg]);
            }
        }
    }

    public function create($idp){
    	$pg=Pegawai::find($idp);
    	return view('anak/create',['pg'=>$pg]);
    }

    public function store($idp, Request $req){

        $req->validate([

            'status'=>'required',
            'nama_anak'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'bulan_lahir'=>'required',
            'tahun_lahir'=>'required',

        ]);
        $anak=new Anak;
        $anak->id_anak=$this->countID();
        $anak->id_pegawai=$idp;
        $anak->status=$req->status;
        $anak->jenjang=$req->jenjang;
        $anak->NISN=$req->NISN;
        $anak->nama_anak=$req->nama_anak;
        $anak->jenis_kelamin=$req->jenis_kelamin;
        $anak->tempat_lahir=$req->tempat_lahir;
        $anak->tanggal_lahir=$req->tahun_lahir."-".$req->bulan_lahir."-".$req->tanggal_lahir;

        $anak->save();

        return redirect('pegawai/'.$idp.'/anak')->with('success','Berhasil Menambah Data');
    }

    public function edit($idp,$ida){
        $anak=Anak::find($ida);
        $pg=Pegawai::find($idp);

        $d_tgl_lahir=substr($anak->tanggal_lahir,8);
        $m_tgl_lahir=substr($anak->tanggal_lahir,5,2);
        $y_tgl_lahir=substr($anak->tanggal_lahir,0,4);
        
        $tanggal_lahir=array(
            "d"=>$d_tgl_lahir,
            "m"=>$m_tgl_lahir,
            "y"=>$y_tgl_lahir,
            );

        return view('anak/edit',compact('anak','pg','tanggal_lahir'));
    }

    public function update($idp,$ida, Request $req){

        $req->validate([

            'status'=>'required',
            'nama_anak'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'bulan_lahir'=>'required',
            'tahun_lahir'=>'required',

        ]);

        $anak=Anak::where('id_anak',$ida)->where('id_pegawai',$idp)->first();
        $anak->status=$req->status;
        $anak->jenjang=$req->jenjang;
        $anak->NISN=$req->NISN;
        $anak->nama_anak=$req->nama_anak;
        $anak->jenis_kelamin=$req->jenis_kelamin;
        $anak->tempat_lahir=$req->tempat_lahir;
        $anak->tanggal_lahir=$req->tahun_lahir."-".$req->bulan_lahir."-".$req->tanggal_lahir;

        $anak->save();

        //return redirect('pegawai/'.$idp.'/anak/'.$ida);
        return back()->with(['success'=>'Berhasil mengubah']);  
    }

    public function delete(Request $req)
    {
        $anak=Anak::where('id_anak',$req->id_anak)->where('id_pegawai',$req->id_pegawai);
        $anak->delete();
        
        //return redirect('pegawai/'.$idp.'/anak');
        return back()->with('success','Berhasil Menghapus Data');  

    }



//----------------------------------

    public function countID() {
   
        $kd= Anak::select()->max('id_anak');
       
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,4,8);
                $newkode = $sub + 1;
                $ID = "ANK-".sprintf('%04s',$newkode);
            }
        }else
        {
            $ID="ANK-0001";

        }
        return $ID;
    }

}
