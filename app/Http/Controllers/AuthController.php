<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Pegawai;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use App\Mail\MailReset;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    //
    public function login()
    {
    	if(!Session::get('login'))
    	{
    		return view('all/login');
    	}
    	else
    	{
    		$user=Session::get('user');
    		if(Session::get('user'))
    		{
    			$pg=Pegawai::find($user);

		        $d_tgl_lahir=substr($pg->tanggal_lahir,8);
		        $m_tgl_lahir=substr($pg->tanggal_lahir,5,2);
		        $y_tgl_lahir=substr($pg->tanggal_lahir,0,4);
		        
		        $tanggal_lahir=array(
		            "d"=>$d_tgl_lahir,
		            "m"=>$m_tgl_lahir,
		            "y"=>$y_tgl_lahir,
		        );

		        $d_TMT_CPNS=substr($pg->TMT_CPNS,8);
		        $m_TMT_CPNS=substr($pg->TMT_CPNS,5,2);
		        $y_TMT_CPNS=substr($pg->TMT_CPNS,0,4);
		        
		        $tmt_cpns=array(
		            "d"=>$d_TMT_CPNS,
		            "m"=>$m_TMT_CPNS,
		            "y"=>$y_TMT_CPNS,
		        );

		        $d_TMT_SK=substr($pg->TMT_SK_pengangkatan,8);
		        $m_TMT_SK=substr($pg->TMT_SK_pengangkatan,5,2);
		        $y_TMT_SK=substr($pg->TMT_SK_pengangkatan,0,4);
		        
		        $tmt_sk_pengangkatan=array(
		            "d"=>$d_TMT_SK,
		            "m"=>$m_TMT_SK,
		            "y"=>$y_TMT_SK,
		        );

		        return view('user/home',compact('pg','tanggal_lahir','tmt_cpns','tmt_sk_pengangkatan'));
    			
    		}
    		else
    		{
    			return view('all/dashboard');
    		}
    	}
    }

    public function loginPost(Request $req)
    {
    	$req->validate([
    		'username' => 'required',
    		'password' => 'required'
    		]);
    	$user=Users::where('username',$req->username)->first();

    	if(count($user)>0)
    	{
    		if(Hash::check($req->password,$user->password))
    		{
    			if($user->status=="user")
    			{
	    			Session::put('user',$user->id_pegawai);
	    			Session::put('login',TRUE);

	    			return redirect('pegawai/'.$user->id_pegawai.'/dashboard');
    			}else
    			{
					Session::put('admin',TRUE);
    				Session::put('login',TRUE);

    				

    				return redirect('');
    			}
    		}else{
    			return back()->with('error','Password Salah !');
    		}

    	}else{
    		return back()->with('error','Anda tidak terdaftar !');
    	}

    }
    public function user($id){
    	if(!Session::get('login')){
    		return redirect('login')->with('error','Silahkan login..');
    	}else{
	    	if(Session::get('user')!=""||Session::get('user')!=null)
	    	{
	    		$user=Session::get('user');
	    		if($id==$user){
			        $pg=Pegawai::find($id);


			        $d_tgl_lahir=substr($pg->tanggal_lahir,8);
			        $m_tgl_lahir=substr($pg->tanggal_lahir,5,2);
			        $y_tgl_lahir=substr($pg->tanggal_lahir,0,4);
			        
			        $tanggal_lahir=array(
			            "d"=>$d_tgl_lahir,
			            "m"=>$m_tgl_lahir,
			            "y"=>$y_tgl_lahir,
			        );

			        $d_TMT_CPNS=substr($pg->TMT_CPNS,8);
			        $m_TMT_CPNS=substr($pg->TMT_CPNS,5,2);
			        $y_TMT_CPNS=substr($pg->TMT_CPNS,0,4);
			        
			        $tmt_cpns=array(
			            "d"=>$d_TMT_CPNS,
			            "m"=>$m_TMT_CPNS,
			            "y"=>$y_TMT_CPNS,
			        );

			        $d_TMT_SK=substr($pg->TMT_SK_pengangkatan,8);
			        $m_TMT_SK=substr($pg->TMT_SK_pengangkatan,5,2);
			        $y_TMT_SK=substr($pg->TMT_SK_pengangkatan,0,4);
			        
			        $tmt_sk_pengangkatan=array(
			            "d"=>$d_TMT_SK,
			            "m"=>$m_TMT_SK,
			            "y"=>$y_TMT_SK,
			        );

			        return view('user/home',compact('pg','tanggal_lahir','tmt_cpns','tmt_sk_pengangkatan'));
			    }
			    else
			    {
			    	abort(404);
			    }
			}

    	}
    }

    public function logout()
    {
    	Session::flush();

    	return redirect('login');
    }

    public function auth($id){
    	$user=Users::where('id_pegawai',$id)->first();
    	$pg=Pegawai::find($id);

    	return view('user/auth',compact('user','pg'));
    }

    public function editAuth(Request $req)
    {
    	$user=Users::where('id_user',$req->id_user)->first();

    	$req->validate([
    			'new_password' => 'required|confirmed|min:6',
    			'new_password_confirmation'=> 'required|min:6',
    			'old_password'=>'required'
    		]);

    	if(Hash::check($req->old_password,$user->password))
    	{
    		$user->password=bcrypt($req->new_password);
    		$user->save();

    		return back()->with('success','Berhasil mengubah password');

    	}else{
    		return back()->with('error','Password salah !');
    	}
    }

    public function lupaPassword()
    {
        return view('all.reset-password');
    }

    public function changePassword(Request $req)
    {
        $pg_count=Pegawai::where('email',$req->email)->count();
        if($pg_count>0)
        {
            $pg=Pegawai::where('email',$req->email)->first();
            $user=Users::where('id_pegawai',$pg->id_pegawai)->first();

            $pass=str_random(6);
            $password=bcrypt($pass);

            $user->password=$password;
            $user->save();


            return $this->sendNewPassword($pg->id_pegawai,$user->username,$pass);
        }else{
            return back()->with('error','Email tidak terdaftar !');
        }
        

        
    }

    public function sendNewPassword($id,$username,$pass)//Kirim password user to Email
    {

        $pg=Pegawai::find($id);
        

        $objMail = new \stdClass();
        $objMail->demo_one = $username;
        $objMail->demo_two = $pass;
        $objMail->sender = 'SMK NEGERI 1 PURWOKERTO';
        $objMail->receiver = strtoupper($pg->nama);
 
        Mail::to($pg->email)->send(new MailReset($objMail));

        return redirect('login')->with('success','Berhasil melakukan lupa akun, cek email anda untuk mendapatkan password baru !');

    }

}
