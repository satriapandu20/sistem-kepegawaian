<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokumen;
use App\Pegawai;
use File;

class DokumenController extends Controller
{
    //
    function index($id)
    {
    	$doc=Dokumen::where('id_pegawai',$id)->get();
    	$pg=Pegawai::find($id);

    	return view('dokumen.home',compact('pg','doc'));
    }

    function index_gaji_berkala($id)
    {
        $doc=Dokumen::where('id_pegawai',$id)->where('jenis','gaji_berkala')->get();
        $pg=Pegawai::find($id);

        return view('dokumen.home',compact('pg','doc'));
    }
    function index_lain($id)
    {
        $doc=Dokumen::where('id_pegawai',$id)->where('jenis','lainnya')->get();
        $pg=Pegawai::find($id);

        return view('dokumen.home',compact('pg','doc'));
    }

    function create($id)
    {
    	$pg=Pegawai::find($id);

    	return view('dokumen.create',compact('pg'));
    }

    function store( Request $req,$id)
    {

    	$req->validate([
    		'nama_dokumen'=>'required',
    		'dokumen'=>'required|max:10000',
            'jenis'=>'required'
    	]);

    	$doc=new Dokumen();
    	$doc->id_pegawai=$id;
    	$doc->nama_dokumen=$req->nama_dokumen;
        $doc->jenis=$req->jenis;
    	$doc->keterangan=$req->keterangan;

        $file=$req->file('dokumen');
        
        $new_name=rand() .'.'.$file->getClientOriginalExtension();

        $file->move(public_path('doc'),$new_name);

        $doc->label=$new_name;

    	$doc->save();
        if($req->jenis=="gaji_berkala"){
    	return redirect('pegawai/'.$id.'/dokumen_gaji_berkala');
        }else{
        return redirect('pegawai/'.$id.'/dokumen_lain');     
        }

    }

    function edit($idp,$id)
    {
    	$doc=Dokumen::where('id_dokumen',$id)->where('id_pegawai',$idp)->first();
    	$pg=Pegawai::find($idp);

    	return view('dokumen.edit',compact('doc','pg'));
    }
    function update($idp,$id,Request $req)
    {
    	$req->validate([
    		'nama_dokumen'=>'required',
            'jenis'=>'required'
    	]);

    	$doc=Dokumen::where('id_dokumen',$id)->where('id_pegawai',$idp)->first();
    	$doc->nama_dokumen=$req->nama_dokumen;
        $doc->jenis=$req->jenis;

    	if($req->file('dokumen')!="")
    	{
    		$path=public_path('doc/'.$doc->label);

    		if(File::exists($path))
    		{
    			File::delete($path);
    		}

    		$file=$req->file('dokumen');
        
        	$new_name=rand() .'.'.$file->getClientOriginalExtension();

        	$file->move(public_path('doc'),$new_name);

       		$doc->label=$new_name;

    	}

    	$doc->keterangan=$req->keterangan;

    	$doc->save();



    	return back()->with('success','Berhasil menyimpan perubahan');


    }

    function delete(Request $req)
    {
    	$doc=Dokumen::where('id_pegawai',$req->id_pegawai)->where('id_dokumen',$req->id_dokumen)->first();

    	$path=public_path('doc/'.$doc->label);

    		if(File::exists($path))
    		{
    			File::delete($path);
    		}
    	
    	$doc->delete();

    	return back()->with('success','Berhasil menghapus dokumen');
    }

    function downloadDocument($idp,$id)
    {
        $doc=Dokumen::where('id_pegawai',$idp)->where('id_dokumen',$id)->first();

        $file= public_path('doc/'.$doc->label);
        
        return response()->download($file);

    }
}
