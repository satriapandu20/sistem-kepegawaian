<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatGajiBerkala;
use App\Pegawai;
use App\RiwayatPangkatGolongan;
use App\NotifGajiBerkala;


class RiwayatGajiBerkalaController extends Controller
{
    //
    function index($idp){
    	$rgbs=RiwayatGajiBerkala::join('tb_riwayat_pangkat_golongan','tb_riwayat_gaji_berkala.id_pangkat_golongan','=','tb_riwayat_pangkat_golongan.id_riwayat_pangkat_golongan')->select('tb_riwayat_gaji_berkala.*','tb_riwayat_pangkat_golongan.pangkat_golongan')->where('tb_riwayat_gaji_berkala.id_pegawai',$idp)->get();
    	
    	$pg=Pegawai::find($idp);
        $rpg_count=RiwayatPangkatGolongan::where('id_pegawai',$idp)->count();

    	return view('riwayat_gaji_berkala/home',compact('rgbs','pg','rpg_count'));
    }

    function create($idp){
    	$pg=Pegawai::find($idp);
    	$rpgs=RiwayatPangkatGolongan::where('id_pegawai',$idp)->get();
    	$rpg_count=RiwayatPangkatGolongan::where('id_pegawai',$idp)->count();



    	return view('riwayat_gaji_berkala/create',compact('pg','rpgs','rpg_count'));
    	
    }

    function store($idp,Request $req)
    {
        $req->validate([
            'pangkat_golongan'=>'required',
            'no_sk'=>'required',
            'tahun_sk'=>'required',
            'bulan_sk'=>'required',
            'tanggal_sk'=>'required',
            'tahun_tmt'=>'required',
            'bulan_tmt'=>'required',
            'tanggal_tmt'=>'required',
            'masa_kerja_tahun'=>'required',
            'masa_kerja_bulan'=>'required',
            'gaji_pokok'=>'required',
        ]);
    	$rgb=new RiwayatGajiBerkala;
    	$rgb->id_riwayat_gaji_berkala=$this->countID();
    	$rgb->id_pegawai=$idp;
    	$rgb->id_pangkat_golongan=$req->pangkat_golongan;
    	$rgb->no_sk=$req->no_sk;
    	$rgb->tanggal_sk=$req->tahun_sk."-".$req->bulan_sk."-".$req->tanggal_sk;
    	$rgb->TMT_KGB=$req->tahun_tmt."-".$req->bulan_tmt."-".$req->tanggal_tmt;
    	$rgb->masa_kerja_tahun=$req->masa_kerja_tahun;
    	$rgb->masa_kerja_bulan=$req->masa_kerja_bulan;
    	$rgb->gaji_pokok=$req->gaji_pokok;

    	$rgb->save();

        $ngb= new NotifGajiBerkala;
        $ngb->id_pegawai=$idp;
        $ngb->id_riwayat_gaji_berkala=$rgb->id_riwayat_gaji_berkala;

        $tahun=$req->tahun_tmt+1;
        $waktu_gaji_berkala=$tahun."-".$req->bulan_tmt."-".$req->tanggal_tmt;

        $ngb->waktu_gaji_berkala=$waktu_gaji_berkala;

        $ngb->save();



    	return redirect('pegawai/'.$idp.'/riwayat_gaji_berkala')->with('success','Berhasil Menambah');

    }

    function edit($idp,$idrgb)
    {
    	$pg=Pegawai::find($idp);
    	$rgb=RiwayatGajiBerkala::where('id_pegawai',$idp)->where('id_riwayat_gaji_berkala',$idrgb)->first();
    	$rpgs=RiwayatPangkatGolongan::where('id_pegawai',$idp)->orderBy('pangkat_golongan','asc')->get();
    	
    	$d_tgl_sk=substr($rgb->tanggal_sk,8);
    	$m_tgl_sk=substr($rgb->tanggal_sk,5,2);
    	$y_tgl_sk=substr($rgb->tanggal_sk,0,4);
    	
    	$tgl_sk=array(
    		"d"=>$d_tgl_sk,
    		"m"=>$m_tgl_sk,
    		"y"=>$y_tgl_sk,
    		);

    	$d_tmt_gb=substr($rgb->TMT_KGB,8);
    	$m_tmt_gb=substr($rgb->TMT_KGB,5,2);
    	$y_tmt_gb=substr($rgb->TMT_KGB,0,4);

    	$tmt_gb=array(
    		"d"=>$d_tmt_gb,
    		"m"=>$m_tmt_gb,
    		"y"=>$y_tmt_gb,
    		);

    	return view('riwayat_gaji_berkala/edit',compact('rgb','pg','rpgs','tgl_sk','tmt_gb'));
    }

    function update($idp,$idrgb,Request $req){

        $req->validate([
            'pangkat_golongan'=>'required',
            'no_sk'=>'required',
            'tahun_sk'=>'required',
            'bulan_sk'=>'required',
            'tanggal_sk'=>'required',
            'tahun_tmt'=>'required',
            'bulan_tmt'=>'required',
            'tanggal_tmt'=>'required',
            'masa_kerja_tahun'=>'required',
            'masa_kerja_bulan'=>'required',
            'gaji_pokok'=>'required',
        ]);
    	$rgb=RiwayatGajiBerkala::where('id_pegawai',$idp)->where('id_riwayat_gaji_berkala',$idrgb)->first();
    	
    	$rgb->id_pangkat_golongan=$req->pangkat_golongan;
    	$rgb->no_sk=$req->no_sk;
    	$rgb->tanggal_sk=$req->tahun_sk."-".$req->bulan_sk."-".$req->tanggal_sk;
    	$rgb->TMT_KGB=$req->tahun_tmt."-".$req->bulan_tmt."-".$req->tanggal_tmt;
    	$rgb->masa_kerja_tahun=$req->masa_kerja_tahun;
    	$rgb->masa_kerja_bulan=$req->masa_kerja_bulan;
    	$rgb->gaji_pokok=$req->gaji_pokok;

    	$rgb->save();

        $ngb= new NotifGajiBerkala;
        $ngb->id_pegawai=$idp;
        $ngb->id_riwayat_gaji_berkala=$rgb->id_riwayat_gaji_berkala;

        $tahun=$req->tahun_tmt+1;
        $waktu_gaji_berkala=$tahun."-".$req->bulan_tmt."-".$req->tanggal_tmt;

        $ngb->waktu_gaji_berkala=$waktu_gaji_berkala;

        $ngb->save();

    	return back()->with('success','Berhasil mengubah data');

    }

    function delete(Request $req)
    {
    	$rgb=RiwayatGajiBerkala::where('id_pegawai',$req->id_pegawai)->where('id_riwayat_gaji_berkala',$req->id_riwayat_gaji_berkala)->first();

    	$rgb->delete();

    	return back()->with('success','Berhasil menghapus data');
    }


    //----------------------------------

    public function countID() {
   
        $kd= RiwayatGajiBerkala::select()->max('id_riwayat_gaji_berkala');
       	
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,4,8);
                $newkode = $sub + 1;
                $ID = "RGB-".sprintf('%05s',$newkode);
            }
        }else
        {
            $ID="RGB-00001";

        }
        return $ID;
    }
    
}
