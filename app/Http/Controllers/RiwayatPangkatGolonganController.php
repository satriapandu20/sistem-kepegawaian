<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\RiwayatPangkatGolongan;
use App\RiwayatGajiBerkala;

class RiwayatPangkatGolonganController extends Controller
{
    //
    function index($idp)
    {
    	$pg=Pegawai::find($idp);
    	$rpg=RiwayatPangkatGolongan::where('id_pegawai',$idp)->get();

    	return view('riwayat_pangkat_golongan/home',['pg'=>$pg],['rpgs'=>$rpg]);

    }

    function create($idp)
    {
    	$pg=Pegawai::find($idp);

    	return view('riwayat_pangkat_golongan/create',['pg'=>$pg]);
    }

    function store($idp,Request $req)
    {

        $req->validate([
            'pangkat_golongan'=>'required',
            'no_sk'=>'required',
            'tahun_sk'=>'required',
            'bulan_sk'=>'required',
            'tanggal_sk'=>'required',
            'tahun_tmt'=>'required',
            'bulan_tmt'=>'required',
            'masa_kerja_tahun'=>'required',
            'masa_kerja_bulan'=>'required',
            'lembaga_pengangkat'=>'required'
        ]);


    	$rpg= new RiwayatPangkatGolongan;
    	$rpg->id_riwayat_pangkat_golongan=$this->countID();
    	$rpg->id_pegawai=$idp;

    	$rpg->pangkat_golongan=$req->pangkat_golongan;
    	$rpg->no_sk=$req->no_sk;
    	$rpg->tanggal_sk=$req->tahun_sk."-".$req->bulan_sk."-".$req->tanggal_sk;

    	$rpg->TMT_pangkat_golongan=$req->tahun_tmt."-".$req->bulan_tmt."-".$req->tanggal_tmt;
    	$rpg->masa_kerja_tahun=$req->masa_kerja_tahun;
    	$rpg->masa_kerja_bulan=$req->masa_kerja_bulan;

        $rpg->lembaga_pengangkat=$req->lembaga_pengangkat;

    	$rpg->save();

    	return redirect('pegawai/'.$idp.'/riwayat_pangkat_golongan')->with('success','Berhasil Menambah');
    }

    function edit($idp,$idrpg)
    {

    	$rpg=RiwayatPangkatGolongan::where('id_pegawai',$idp)->where('id_riwayat_pangkat_golongan',$idrpg)->first();
    	$pg=Pegawai::find($idp);

    	$d_tgl_sk=substr($rpg->tanggal_sk,8);
    	$m_tgl_sk=substr($rpg->tanggal_sk,5,2);
    	$y_tgl_sk=substr($rpg->tanggal_sk,0,4);
    	
    	$tgl_sk=array(
    		"d"=>$d_tgl_sk,
    		"m"=>$m_tgl_sk,
    		"y"=>$y_tgl_sk,
    		);
    	
    	//[[====]]
    	$d_tmt_pg=substr($rpg->TMT_pangkat_golongan,8);
    	$m_tmt_pg=substr($rpg->TMT_pangkat_golongan,5,2);
    	$y_tmt_pg=substr($rpg->TMT_pangkat_golongan,0,4);

    	$tmt_pg=array(
    		"d"=>$d_tmt_pg,
    		"m"=>$m_tmt_pg,
    		"y"=>$y_tmt_pg,
    		);


    	return view('riwayat_pangkat_golongan/edit',compact('rpg','pg','tgl_sk','tmt_pg'));

    }

    function update($idp,$idrpg,Request $req)
    {
        $req->validate([
            'pangkat_golongan'=>'required',
            'no_sk'=>'required',
            'tahun_sk'=>'required',
            'bulan_sk'=>'required',
            'tanggal_sk'=>'required',
            'tahun_tmt'=>'required',
            'bulan_tmt'=>'required',
            'masa_kerja_tahun'=>'required',
            'masa_kerja_bulan'=>'required',
            'lembaga_pengangkat'=>'required'
        ]);
    	$rpg=RiwayatPangkatGolongan::where('id_pegawai',$idp)->where('id_riwayat_pangkat_golongan',$idrpg)->first();



    	$rpg->pangkat_golongan=$req->pangkat_golongan;
    	$rpg->no_sk=$req->no_sk;
    	$rpg->tanggal_sk=$req->tahun_sk."-".$req->bulan_sk."-".$req->tanggal_sk;

    	$rpg->TMT_pangkat_golongan=$req->tahun_tmt."-".$req->bulan_tmt."-".$req->tanggal_tmt;
    	$rpg->masa_kerja_tahun=$req->masa_kerja_tahun;
    	$rpg->masa_kerja_bulan=$req->masa_kerja_bulan;

    	$rpg->save();

    	return redirect()->back();

    }

    function delete(Request $req)
    {
        $rgb=RiwayatGajiBerkala::where('id_pegawai',$req->id_pegawai)->where('id_pangkat_golongan',$req->id_riwayat_pangkat_golongan)->first();
        $rgb->delete();

    	$rpg=RiwayatPangkatGolongan::where('id_pegawai',$req->id_pegawai)->where('id_riwayat_pangkat_golongan',$req->id_riwayat_pangkat_golongan)->first();
    	$rpg->delete();

    	return back()->with('success','Berhasil menghapus data pangkat golongan');
    }

    //=========================

    public function countID() {
   
        $kd= RiwayatPangkatGolongan::select()->max('id_riwayat_pangkat_golongan');
       	
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,4,9);
                $newkode = $sub + 1;
                $ID = "RPG-".sprintf('%05s',$newkode);
            }
        }else
        {
            $ID="RPG-00001";

        }
        return $ID;
    }
}
