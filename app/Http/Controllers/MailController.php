<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\MailPensiun;
use App\Mail\MailGaji;
use App\Pegawai;

use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    //
    public function SendPensiunMail($id)
    {

    	$pg=Pegawai::find($id);

        $objMail = new \stdClass();
        $objMail->demo_one = $pg->tahun_pensiun;
        
        $objMail->sender = 'SMK NEGERI 1 PURWOKERTO';
        $objMail->receiver = strtoupper($pg->nama);
 
        Mail::to($pg->email)->send(new MailPensiun($objMail));

        return back()->with('success','Berhasil mengirim notifikasi pensiun email ke : '.strtoupper($pg->nama));
    }

    public function SendGajiMail($id)
    {

        $pg=Pegawai::find($id);
        $y=date('Y');
        $m=date('m');

        


        if($m>9)
        {
            if($m==10){
                $m=00;
                $m=$m+1;
            }
            else if($m==11)
            { 
                $m=00;
                $m=$m+2;
            }
            else{

                $m=00;
                $m=$m+3;

            }
            
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $y=$y+1;
            
        }
        else
        {
            $m=$m+3;
            if(strlen($m)==1){
                $m='0'.$m;
            }
            
            
        }

                    if($m=="01")
                    {
                        $m='Januari';
                    }
                    else if($m=="02")
                    {
                        $m='Februari';
                    }
                    else if($m=="03")
                    {
                        $m='Maret';
                    }
                    else if($m=="04")
                    {
                        $m='April';
                    }
                    else if($m=="05")
                    {
                        $m='Mei';
                    }
                    else if($m=="06")
                    {
                        $m='Juni';
                    }
                    else if($m=="07")
                    {
                        $m='Juli';
                    }
                    else if($m=="08")
                    {
                        $m='Agustus';
                    }
                    else if($m=="09")
                    {
                        $m='September';
                    }
                    else if($m=="10")
                    {
                        $m='Oktober';
                    }
                    else if($m=="11")
                    {
                        $m='November';
                    }
                    else if($m=="12")
                    {
                        $m='Desember';
                    }

        $objMail = new \stdClass();
        $objMail->demo_one = $y;
        $objMail->demo_two = $m;
        $objMail->sender = 'SMK NEGERI 1 PURWOKERTO';
        $objMail->receiver = strtoupper($pg->nama);
 
        Mail::to($pg->email)->send(new MailGaji($objMail));

        return back()->with('success','Berhasil mengirim notifikasi gaji berkala email ke : '.strtoupper($pg->nama));
    }

}
