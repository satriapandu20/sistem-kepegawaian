<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diklat;
use App\Pegawai;

use Illuminate\Support\Facades\Session;

class DiklatController extends Controller
{
    //
    function index($idp)
    {
        if(!Session::get('login')){
            return redirect('login')->with('error','Silahkan login..');
        }else{
            if(Session::get('user')!=""||Session::get('user')!=null)
            {
                $user=Session::get('user');
                if($idp==$user){
                	$dk=Diklat::where('id_pegawai',$idp)->get();
                	$pg=Pegawai::find($idp);



                	return view('diklat/home',['dk'=>$dk],['pg'=>$pg]);
                }else{
                    abort(404);
                }
            }else if(Session::get('admin')!=""||Session::get('admin')!=null){
                $dk=Diklat::where('id_pegawai',$idp)->get();
                    $pg=Pegawai::find($idp);



                    return view('diklat/home',['dk'=>$dk],['pg'=>$pg]);
            }
        }
    }

    function create($idp)
    {
    	$pg=Pegawai::find($idp);

    	return view('diklat/create',['pg'=>$pg]);
    }

    function store($idp,Request $req)
    {
        $req->validate([
            'jenis_diklat'=>'required',
            'nama_diklat'=>'required',
            'penyelenggara'=>'required',
            'tahun'=>'required',
            'peran'=>'required'

        ]);
    	$dk=new Diklat;
    	$dk->id_diklat=$this->countID();
    	$dk->id_pegawai=$idp;
    	$dk->jenis_diklat=$req->jenis_diklat;
    	$dk->nama_diklat=$req->nama_diklat;
    	$dk->penyelenggara=$req->penyelenggara;
    	$dk->tahun=$req->tahun;
    	$dk->peran=$req->peran;

    	$dk->save();

    	return redirect('pegawai/'.$idp.'/diklat')->with('success','Berhasil menambah data');
    }

    function edit($idp,$idk)
    {
    	$pg=Pegawai::find($idp);
    	$dk=Diklat::where('id_diklat',$idk)->where('id_pegawai',$idp)->first();

    	return view('diklat/edit',['dk'=>$dk],['pg'=>$pg]);
    }

    function update($idp,$idk, Request $req)
    {
         $req->validate([
            'jenis_diklat'=>'required',
            'nama_diklat'=>'required',
            'penyelenggara'=>'required',
            'tahun'=>'required',
            'peran'=>'required'

        ]);
    	$dk=Diklat::where('id_diklat',$idk)->where('id_pegawai',$idp)->first();

    	$dk->jenis_diklat=$req->jenis_diklat;
    	$dk->nama_diklat=$req->nama_diklat;
    	$dk->penyelenggara=$req->penyelenggara;
    	$dk->tahun=$req->tahun;
    	$dk->peran=$req->peran;

    	$dk->save();

    	return redirect()->back()->with('success','Berhasil mengubah data');
    }

    function delete(Request $req)
    {
    	$dk=Diklat::where('id_diklat',$req->id_diklat)->where('id_pegawai',$req->id_pegawai)->first();
    	$dk->delete();

    	return back()->with('success','Berhasil menghapus data');
    }

    //----------------------------------

    public function countID() {
   
        $kd= Diklat::select()->max('id_diklat');
       	
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,4,8);
                $newkode = $sub + 1;
                $ID = "DKT-".sprintf('%05s',$newkode);
            }
        }else
        {
            $ID="DKT-00001";

        }
        return $ID;
    }
}
