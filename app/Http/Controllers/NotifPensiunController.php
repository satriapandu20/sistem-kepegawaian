<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotifPensiun;
use App\Pegawai;

class NotifPensiunController extends Controller
{
    //
    public function index(){
        $tahun=date('Y');
        $tahun=$tahun+1;
     
        $np=NotifPensiun::join('tb_pegawai','tb_history_notif_pensiun.id_pegawai','=','tb_pegawai.id_pegawai')
        				->where('tb_pegawai.tahun_pensiun',$tahun)->get();
        
        
        return view('notify.notif-pensiun',compact('tahun','np'));
    }

}
