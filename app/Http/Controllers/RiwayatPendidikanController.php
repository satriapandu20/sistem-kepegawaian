<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPendidikan;
use App\Pegawai;

class RiwayatPendidikanController extends Controller
{
    //
    function index($idp){
    	$pg=Pegawai::find($idp);
    	$rwp=RiwayatPendidikan::where('id_pegawai',$idp)->get();

    	return view('riwayat_pendidikan/home',['pg'=>$pg],['rwps'=>$rwp]);


    }
    function create($idp){
    	$pg=Pegawai::find($idp);

    	return view('riwayat_pendidikan/create',['pg'=>$pg]);
    }

    function store($idp, Request $req){

        $req->validate([
            'jenjang'=>'required',
            'nama_sekolah'=>'required',
            'tahun_masuk'=>'required|max:4|min:4',
            'tahun_lulus'=>'required|max:4|min:4',
            'NIM'=>'required',
            'status'=>'required',
            'semester'=>'required'
        ]);

    	$rwp=new RiwayatPendidikan;
    	$rwp->id_riwayat_pendidikan=$this->countID();
    	$rwp->id_pegawai=$idp;
    	$rwp->bidang_studi=$req->bidang_studi;
    	$rwp->jenjang=$req->jenjang;
    	$rwp->gelar=$req->gelar;
    	$rwp->nama_sekolah=$req->nama_sekolah;
    	$rwp->fakultas=$req->fakultas;
    	$rwp->kependidikan=$req->kependidikan;
    	$rwp->tahun_masuk=$req->tahun_masuk;
    	$rwp->tahun_lulus=$req->tahun_lulus;
    	$rwp->NIM=$req->NIM;
    	$rwp->status=$req->status;
    	$rwp->semester=$req->semester;
    	$rwp->IPK=$req->IPK;

    	$rwp->save();

    	return redirect('pegawai/'.$idp.'/riwayat_pendidikan')->with('success','Berhasil menambah data');
    }

    function edit($idp, $idrwp)
    {
    	$rwp=RiwayatPendidikan::where('id_pegawai',$idp)->where('id_riwayat_pendidikan',$idrwp)->first();
    	

    	$pg=Pegawai::find($idp);

    	return view('riwayat_pendidikan/edit',['rwp'=>$rwp],['pg'=>$pg]);
    }

    function update($idp, $idrwp, Request $req)
    {
        $req->validate([
            'jenjang'=>'required',
            'nama_sekolah'=>'required',
            'tahun_masuk'=>'required|max:4|min:4',
            'tahun_lulus'=>'required|max:4|min:4',
            'NIM'=>'required',
            'status'=>'required',
            'semester'=>'required'
        ]);

    	$rwp=RiwayatPendidikan::where('id_pegawai',$idp)->where('id_riwayat_pendidikan',$idrwp)->first();   	
    	$rwp->bidang_studi=$req->bidang_studi;
    	$rwp->jenjang=$req->jenjang;
    	$rwp->gelar=$req->gelar;
    	$rwp->nama_sekolah=$req->nama_sekolah;
    	$rwp->fakultas=$req->fakultas;
    	$rwp->kependidikan=$req->kependidikan;
    	$rwp->tahun_masuk=$req->tahun_masuk;
    	$rwp->tahun_lulus=$req->tahun_lulus;
    	$rwp->NIM=$req->NIM;
    	$rwp->status=$req->status;
    	$rwp->semester=$req->semester;
    	$rwp->IPK=$req->IPK;

    	$rwp->save();

    	return redirect()->back()->with('success','Behrasil mengubah data');

    }

    function delete(Request $req)
    {
    	$rwp=RiwayatPendidikan::where('id_pegawai',$req->id_pegawai)->where('id_riwayat_pendidikan',$req->id_riwayat_pendidikan)->first();

    	$rwp->delete();

    	return redirect('pegawai/'.$req->id_pegawai.'/riwayat_pendidikan')->with('success','Berhasil menghapus data');
    }
    //----------------------------------

    public function countID() {
   
        $kd= RiwayatPendidikan::select()->max('id_riwayat_pendidikan');
       	
        
        if($kd!=null)
        {

            foreach ((array)$kd as $row) {

                $sub = substr($row,4,8);
                $newkode = $sub + 1;
                $ID = "RWP-".sprintf('%05s',$newkode);
            }
        }else
        {
            $ID="RWP-00001";

        }
        return $ID;
    }
}
