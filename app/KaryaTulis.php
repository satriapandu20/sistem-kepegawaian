<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KaryaTulis extends Model
{
    //
    protected $table="tb_karya_tulis";
    protected $primaryKey="id_karya";
    public $incrementing=false;

    protected $fillable=['id_karya','id_pegawai','judul','tahun','publikasi','keterangan'];
}
