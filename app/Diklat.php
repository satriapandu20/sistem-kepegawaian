<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diklat extends Model
{
    //
    protected $table='tb_diklat';
    protected $primaryKey='id_diklat';
    public $incrementing=false;

    protected $fillable=['id_diklat','id_pegawai','jenis_diklat','nama_diklat','penyelenggara','tahun','peran'];
}
