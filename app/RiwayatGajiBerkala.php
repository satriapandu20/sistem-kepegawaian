<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatGajiBerkala extends Model
{
    //
    protected $table='tb_riwayat_gaji_berkala';
    protected $primaryKey='id_riwayat_gaji_berkala';
    public $incrementing=false;

    protected $fillable=['id_riwayat_gaji_berkala','id_pegawai','id_pangkat_golongan','no_sk','tanggal_sk','TMT_KGB','masa_kerja_tahun','masa_kerja_bulan','gaji_pokok'];
}
