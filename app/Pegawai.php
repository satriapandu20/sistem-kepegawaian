<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table= 'tb_pegawai';
    protected $primaryKey = 'id_pegawai';
    public $incrementing = false;
   
    protected $fillable=['id_pegawai','nama','nik','jenis_kelamin','tempat_lahir','tanggal_lahir','nama_ibu','alamat','rt','rw','kelurahan','kecamatan','kabupaten','kode_pos','agama','status_perkawinan','nama_pasangan','pekerjaan_pasangan','npwp','nama_wajib_pajak','nuptk','nip','sk_cpns','tmt_sk_pengangkatan','sumber_gaji','pangkat_golongan','masa_kejar','sk_pengangkatan','tmt_pengangkatan','lembaga_pengangkatan','kartu_pegawai','kartu_pasangan','kompentensi','pendidikan_terakhir','email','tahun_pensiun'];

  
}
