<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifPensiun extends Model
{
    //
    protected $table="tb_history_notif_pensiun";
    protected $primaryKey="id_history_notif_pensiun";
    

    protected $fillable=['id_history_notif_pensiun','id_pegawai','status'];
}
