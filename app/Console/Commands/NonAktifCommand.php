<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Pegawai;

class NonAktifCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pegawai:non_aktif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Non Aktif Pegawai';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $tahun=date('Y');

        $pg=Pegawai::where('tahun_pensiun',$tahun)->first();

        $pg->status_keaktifan='Tidak Aktif';
        $pg->save();

    }
}
