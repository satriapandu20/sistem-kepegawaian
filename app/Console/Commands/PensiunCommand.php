<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NotifPensiun;
use App\Pegawai;

use App\Mail\MailPensiun;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class PensiunCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:pensiun';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifikasi Pensiun Pegawai';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $tahun=date('Y');
        $tahun=$tahun+1;
     
        $np=NotifPensiun::join('tb_pegawai','tb_history_notif_pensiun.id_pegawai','=','tb_pegawai.id_pegawai')
                        ->where('tb_pegawai.tahun_pensiun',$tahun)->get();
        foreach ($np as $np) {
            # code...
            $objMail = new \stdClass();
            $objMail->demo_one = $np->tahun_pensiun;
        
            $objMail->sender = 'SMK NEGERI 1 PURWOKERTO';
            $objMail->receiver = strtoupper($np->nama);
     
            Mail::to($np->email)->send(new MailPensiun($objMail));

            Nexmo::message()->send([
            'to'=>'62'.$np->nomor_telepon,
            'from'=>'SMKN 1 PURWOKERTO',
            'text' => 'Anda akan pensiun pada tahun '.$np->tahun_pensiun.', segera siapkan berkas-berkas anda.

Terimakasih, SMKN 1 PURWOKERTO'
            ]);

            $np_edit=NotifPensiun::find($np->id_history_notif_pensiun);

            $np_edit->status='done';
            $np_edit->save();

        }
    }
}
