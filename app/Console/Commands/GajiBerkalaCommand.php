<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NotifGajiBerkala;
use App\Pegawai;
use App\RiwayatGajiBerkala;

use App\Mail\MailGaji;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class GajiBerkalaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:gaji_berkala';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifikasi Gaji Berkala';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $y=date('Y');
        $m=date('m');
        
        $yTerakhir_Naik_Gaji;


        if($m>9)
        {
            if($m==10){
                $m=00;
                $m=$m+1;
            }
            else if($m==11)
            { 
                $m=00;
                $m=$m+2;
            }
            else{

                $m=00;
                $m=$m+3;

            }
            
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $y=$y+1;
            $yTerakhir_Naik_Gaji=$y-1;
        }
        else
        {
            $m=$m+3;
            if(strlen($m)==1){
                $m='0'.$m;
            }
            $yTerakhir_Naik_Gaji=$y-1;
            
        }


        $lastKGB=$yTerakhir_Naik_Gaji.'-'.$m;
        $currentKGB=$y.'-'.$m;

        $ngb=NotifGajiBerkala::join('tb_pegawai','tb_history_notif_gaji_berkala.id_pegawai','=','tb_pegawai.id_pegawai')
            ->join('tb_riwayat_gaji_berkala','tb_history_notif_gaji_berkala.id_riwayat_gaji_berkala','=','tb_riwayat_gaji_berkala.id_riwayat_gaji_berkala')
            ->where('waktu_gaji_berkala','like','%'.$currentKGB.'%')->get();

                    if($m=="01")
                    {
                        $m='Januari';
                    }
                    else if($m=="02")
                    {
                        $m='Februari';
                    }
                    else if($m=="03")
                    {
                        $m='Maret';
                    }
                    else if($m=="04")
                    {
                        $m='April';
                    }
                    else if($m=="05")
                    {
                        $m='Mei';
                    }
                    else if($m=="06")
                    {
                        $m='Juni';
                    }
                    else if($m=="07")
                    {
                        $m='Juli';
                    }
                    else if($m=="08")
                    {
                        $m='Agustus';
                    }
                    else if($m=="09")
                    {
                        $m='September';
                    }
                    else if($m=="10")
                    {
                        $m='Oktober';
                    }
                    else if($m=="11")
                    {
                        $m='November';
                    }
                    else if($m=="12")
                    {
                        $m='Desember';
                    }

        foreach($ngb as $ngb) {
            $objMail = new \stdClass();
            $objMail->demo_one = $y;
            $objMail->demo_two = $m;
            $objMail->sender = 'SMK NEGERI 1 PURWOKERTO';
            $objMail->receiver = strtoupper($ngb->nama);
 
            Mail::to($ngb->email)->send(new MailGaji($objMail));

            

            Nexmo::message()->send([
            'to'=>'62'.$ngb->nomor_telepon,
            'from'=>'SMKN 1 PURWOKERTO',
            'text' => 'Pemberitahuan kepada '.$ngb->nama.',
Anda akan naik gaji pada bulan '.$m.' tahun '.$y.', segera persiapkan berkas-berkas anda.

Terimakasih, SMKN 1 PURWOKERTO'
        ]);

            $ngb_edit=NotifGajiBerkala::find($ngb->id_history_notif_gaji_berkala);

            $ngb_edit->status="done";
            $ngb_edit->save();
        }
        

    }
}
