<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{
    //
    protected $table='tb_anak';
    protected $primaryKey='id_anak';
    public $incrementing = false;

    protected $fillable=['id_anak','id_pegawai','status','jenjang','NISN','nama_anak','jenis_kelamin','tempat_lahir','tanggal_lahir'];

}
