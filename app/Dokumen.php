<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    //
    protected $table='tb_dokumen';
    protected $primaryKey='id_dokumen';

    protected $fillable=['id_dokumen','id_pegawai','nama_dokumen','label','keterangan'];
    
}
