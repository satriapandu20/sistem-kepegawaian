<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model
{
    //
    protected $table='tb_riwayat_pendidikan';
    protected $primaryKey='id_riwayat_pendidikan';
    public $incrementing=false;

    protected $fillable=['id_riwayat_pendidikan','id_pegawai','bidang_studi','jenjang','gelar','nama_sekolah','fakultas','kependidikan','tahun_masuk','tahun_lulus','NIM','status','semester','IPK'];
}
