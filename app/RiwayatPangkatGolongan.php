<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPangkatGolongan extends Model
{
    //

    protected $table="tb_riwayat_pangkat_golongan";
    protected $primaryKey="id_riwayat_pangkat_golongan";
    public $incrementing=false;

    protected $fillable=['id_riwayat_pangkat_golongan','id_pegawai','pangkat_golongan','no_sk','tanggal_sk','TMT_pangkat_golongan','masa_kerja_tahun','masa_kerja_bulan'];
}
