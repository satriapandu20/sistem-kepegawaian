<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifGajiBerkala extends Model
{
    //
    protected $table="tb_history_notif_gaji_berkala";
    protected $primaryKey="id_history_notif_gaji_berkala";
    

    protected $fillable=['id_history_notif_gaji_berkala','id_pegawai','id_riwayat_gaji_berkala','waktu_gaji_berkala','status'];
}
