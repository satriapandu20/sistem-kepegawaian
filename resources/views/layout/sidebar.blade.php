<div class="navbar-default sidebar" role="navigation">

    <div class="sidebar-nav navbar-collapse">

        <ul class="nav" id="side-menu">

            <!-- <li class="sidebar-search">

                <div class="input-group custom-search-form">

                    <input type="text" class="form-control" placeholder="Search...">

                    <span class="input-group-btn">

                    <button class="btn btn-default" type="button">

                        <i class="fa fa-search"></i>

                    </button>

                </span>

                </div>

                

            </li> -->
            <!-- /input-group -->
            <?php 
                if(Session::get('admin'))
                {

            ?>

            <li>

                <a href="{{ url('') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>

            </li>


            <li>

                <a href="{{ url('pegawai/') }}"><i class="fa fa-user fa-fw"></i> Daftar Pegawai</a>

            </li>

            <li>

                <a href="{{ url('pegawai/tidak_aktif') }}"><i class="fa fa-user fa-fw"></i> Daftar Pegawai Tidak Aktif</a>

            </li>

            <li>

                <a href="{{ url('pegawai/pensiun')}}"><i class="fa fa-edit fa-fw"></i> Daftar Pensiun </a>

            </li>

            <li>


            <li>

                <a href="{{ url('pegawai/naik_gaji')}}"><i class="fa fa-edit fa-fw"></i> Kenaikan Gaji </a>

            </li>
            <?php 
                }
                else if(Session::get('user'))
                {

             ?>
            <li>

                <a href="{{ url('pegawai/'.Session::get('user').'/dashboard')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>

            </li>

            <li>

                <a href="{{ url('pegawai/'.Session::get('user'))}}"><i class="fa fa-user fa-fw"></i> Data Diri</a>

            </li>

            <li>

                <a href="{{ url('pegawai/'.Session::get('user').'/anak')}}"><i class="fa fa-edit fa-fw"></i> Data Anak </a>

            </li>

            <li>

                <a href="{{ url('pegawai/'.Session::get('user').'/karya')}}"><i class="fa fa-edit fa-fw"></i> Data Karya </a>

            </li>

            <li>

                <a href="{{ url('pegawai/'.Session::get('user').'/diklat')}}"><i class="fa fa-edit fa-fw"></i> Data Diklat </a>

            </li>

            <li>

                <a href="#"><i class="fa fa-calendar fa-fw"></i> Riwayat<span class="fa arrow"></span></a>

                <ul class="nav nav-second-level">

                    <li>

                        <a href="{{ url('pegawai/'.Session::get('user').'/riwayat_pendidikan')}}"> Riwayat Pendidikan</a>

                    </li>

                    <li>

                        <a href="{{ url('pegawai/'.Session::get('user').'/riwayat_pangkat_golongan')}}"> Riwayat Pangkat Golongan</a>

                    </li>

                    <li>

                        <a href="{{ url('pegawai/'.Session::get('user').'/riwayat_gaji_berkala')}}"> Riwayat Gaji Berkala</a>

                    </li>

                </ul>

            </li>

            <li>

                <a href="#"><i class="fa  fa-file-text-o fa-fw"></i> Aturan & Persyaratan<span class="fa arrow"></span></a>

                <ul class="nav nav-second-level">

                    <li>

                        <a href="#" data-toggle="modal" data-target="#aturan_gaji"> Aturan Gaji Berkala</a>

                    </li>

                    <li>

                        <a href="#" data-toggle="modal" data-target="#syarat_gaji"> Persyaratan Gaji Berkala</a>

                    </li>

                    <li>

                        <a href="#" data-toggle="modal" data-target="#aturan_pensiun"> Aturan Pensiun Pegawai</a>

                    </li>

                    <li>

                        <a href="#" data-toggle="modal" data-target="#syarat_pensiun"> Persyaratan Pensiun</a>

                    </li>
                </ul>
            </li>


            <li>

                <a href="#"><i class="fa fa-folder-o fa-fw"></i> Dokumen Saya<span class="fa arrow"></span></a>

                

                <ul class="nav nav-second-level">

                    <li>

                        <a href="{{ url('pegawai/'.Session::get('user')).'/dokumen_gaji_berkala'}}"> Dokumen Gaji Berkala</a>

                    </li>

                    <li>

                        <a href="{{ url('pegawai/'.Session::get('user')).'/dokumen_lain'}}"> Dokumen Lainnya</a>

                    </li>

                </ul>

            </li>


            <?php
                }

            ?>

                            <!-- /.nav-second-level -->

            

           <!--  <li>

                <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>

                <ul class="nav nav-second-level">

                    <li>

                        <a href="panels-wells.html">Panels and Wells</a>

                    </li>

                </ul>
 -->
                <!-- /.nav-second-level -->

            <!-- </li>

            <li>

                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>

                <ul class="nav nav-second-level">

                    <li>

                        <a href="blank.html">Blank Page</a>

                    </li>

                    <li>

                        <a href="login.html">Login Page</a>

                    </li>

                </ul>
 -->
                <!-- /.nav-second-level -->

           <!--  </li> -->

        </ul>
        <br>
    <p style="bottom: 0;" align="center">

    &copy; SMK Negeri 1 Purwokerto
    </p>

    </div>

    <!-- /.sidebar-collapse -->

</div>

<!-- /.navbar-static-side -->
