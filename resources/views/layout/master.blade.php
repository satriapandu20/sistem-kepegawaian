<!DOCTYPE html>
<html>

<head>
	<title>@yield('title')</title>



  	<!-- <link rel="stylesheet" href="{{ url('public/css/bootstrap.min.css') }}" type="text/css" />
  	<link rel="stylesheet" href="{{ url('public/css/bootstrap.css') }}" type="text/css" /> -->


	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	   <!-- Bootstrap Core CSS -->

    <link href="{!! asset('theme/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

    


    <!-- MetisMenu CSS -->

    <link href="{!! asset('theme/vendor/metisMenu/metisMenu.min.css') !!}" rel="stylesheet">



    <!-- Custom CSS -->

    <link href="{!! asset('theme/dist/css/sb-admin-2.css') !!}" rel="stylesheet">



    <!-- Morris Charts CSS -->

    <link href="{!! asset('theme/vendor/morrisjs/morris.css') !!}" rel="stylesheet">



    <!-- Custom Fonts -->

    <link href="{!! asset('theme/vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">


    <link href="{!! asset('public/css/more.css') !!}" rel="stylesheet">

</head>

<body>
<div id="wrapper">

	    <!-- Navigation -->

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: #B22222;" >

        @include('layout.header')

        @include('layout.sidebar')

    </nav>

<!-- 
	<a href="{{ url('/') }}">Beranda</a>
	<a href="{{ url('pegawai') }} ">Pegawai</a>
 -->


	<div id="page-wrapper">

    @include('layout.flash-message')
	@yield('content')

        <!-- Modal Aturan Gaji -->
        <div class="modal fade" id="aturan_gaji" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Aturan Kenaikan Gaji Berkala</h4>
                    </div>
                    <div class="modal-body">
                        <p>Berdasarkan Peraturan Pemerintah Nomor 7 Tahun 1977 tentang Peraturan Gaji Pegawai Negeri Sipil  sebagaimana telah diubah dengan Peraturan Pemerintah Nomor 66 Tahun 2005</p>

                        <p>Kenaikan gaji berkala adalah kenaikan gaji yang diberikan kepada pegawai negeri sipil yang telah mencapai masa kerja golongan yang ditentukan untuk kenaikan gaji berkala yaitu setiap 2 (dua) tahun sekali dan apabila telah memenuhi persyaratan berdasarkan peraturan perundang-undangan yang berlaku</p>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
                                        <!-- /.modal-content -->
            </div>
                                    <!-- /.modal-dialog -->
        </div>

        <!-- Modal Aturan Pensiun -->
        <div class="modal fade" id="aturan_pensiun" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Aturan Pensiun Pegawai</h4>
                    </div>
                    <div class="modal-body" style="text-align: justify;">

                    <p>Mengacu pada Pasal 239, Pasal 240, Pasal 354, dan Pasal 355 Peraturan Pemerintah Nomor:  11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil , menurut surat tersebut telah ditentukan sebagai berikut:</p>
                        <ul>
                            <li>PNS yang telah mencapai Batas Usia Pensiun diberhentikan dengan hormat sebagai PNS.</li>
                            <li>Batas Usia Pensiun sebagaimana dimaksud yaitu:
                             <ol>
                                <li>58 (lima puluh delapan) tahun bagi pejabat administrasi, pejabat fungsional ahli muda, pejabat fungsional ahli pertama, dan pejabat fungsional keterampilan</li>
                                <li>60 (enam puluh) tahun bagi pejabat pimpinan tinggi dan pejabat fungsional madya</li>
                                <li>65 (enam puluh lima) tahun bagi PNS yang memangku pejabat fungsional ahli utama</li>
                             </ol>
                            </li>
                        
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
                                        <!-- /.modal-content -->
            </div>
                                    <!-- /.modal-dialog -->
        </div>

        <!-- Modal Syarat Gaji -->
        <div class="modal fade" id="syarat_gaji" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Persyaratan Kenaikan Gaji Berkala</h4>
                    </div>
                    <div class="modal-body">
                        <ul>
                            <li>Fotokopi SK Kenaikan Gaji Berkala Terakhir</li>
                            <li>Fotokopi SK Kenaikan Pangkat Golongan Terbaru</li>
                            <li>Fotokopi sah kartu pegawai</li>
                            <li>Fotokopi sah Daftar Penilaian Pelaksanaan Pekerjaan (DP-3) terakhir dengan nilai rata-rata cukup</li>
                        
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
                                        <!-- /.modal-content -->
            </div>
                                    <!-- /.modal-dialog -->
        </div>


        <!-- Modal Syarat Pensiun -->
        <div class="modal fade" id="syarat_pensiun" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Persyaratan Berkas Pensiun</h4>
                    </div>
                    <div class="modal-body" style="text-align: justify;">
                        <ol>
                            <li>DPCP (Data Perorangan Calon penerima Pensiun)</li>
                            <li>Fotokopi SK Honor, CPNS, PNS, pangkat & jabatan terakhir berlegalisir</li>
                            <li>Fotokopi Kartu Pegawai berlegalisir</li>
                            <li>Fotokopi Surat kenaikan gaji berkala terakhir berlegalisir</li>
                            <li>Fotokopi Konversi NIP berligalisir</li>
                            <li>Daftar riwayat pekerjaan</li>
                            <li>Daftar susunan keluarga</li>
                            <li>Surat nikah/cerai berlegalisir</li>
                            <li>Surat lahir anak < 25 tahun, belum menikah, belum bekerja & masih sekolah</li>
                            <li>Surat keterangan masih sekolah untuk umur > 21 tahun</li>
                            <li>Surat Keterangan Kematian (Bagi pensiun janda/duda)</li>
                            <li>Surat hasil Tim Penguji Kesehatan yang telah ditunjuk oleh Pemerintah(Untuk Pensiun Keuzuran)</li>
                            <li>Pas photo PNS yang mengusulkan ukuran 4 x 6 = 7 lembar (untuk pensiun BUP dan APS); pas photo suami/istri yang meninggal dunia ukuran 4 x 6 = 7 lembar (pensiun janda/duda)</li>
                            <li>DP3 tahun terakhir atau PPK(Penilaian Prestasi Kerja)</li>
                            <li>Surat keterangan tidak pernah dijatuhi hukuman disiplin berat (Bermaterai dan ditandatangani oleh Kepala BKD atau Inspektur)</li>
                            <li>Surat Keterangan tidak pernah terlibat permasalahan hukum (Bermaterai dan ditandatangani oleh Kepala BKD atau Inspektur)</li>
                    
                        </ol>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
                                        <!-- /.modal-content -->
            </div>
                                    <!-- /.modal-dialog -->
        </div>


	</div>


	
	
	
</div>

    

</body>
</html>


 <!-- jQuery -->
    <script src="{!! asset('theme/vendor/jquery/jquery.min.js') !!}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{!! asset('theme/vendor/bootstrap/js/bootstrap.min.js') !!}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{!! asset('theme/vendor/metisMenu/metisMenu.min.js') !!}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{!! asset('theme/dist/js/sb-admin-2.js') !!}"></script>

    <!-- Javascript untuk popup modal Delete--> 

    <script type="text/javascript">
        
    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
     
        return false;
        return true;
    }
    </script>
