<div class="navbar-header" style="background-color: #B22222;">

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >

        <span class="sr-only">Toggle navigation</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

    </button>

    <a class="navbar-brand" style="color:#fff;" href="{{ url('') }}">SMK NEGERI 1 PURWOKERTO</a>

</div>

<!-- /.navbar-header -->



<ul class="nav navbar-top-links navbar-right">

    

    <!-- /.dropdown -->

    <li class="dropdown">

        <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:#fff;">

            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>

        </a>

        <ul class="dropdown-menu dropdown-user">


            <li>

            <?php 
                if(Session::get('user')){
            ?>

            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/auth') }}"><i class="fa fa-gear fa-fw"></i> Pengaturan</a>

            <?php } ?>

            </li>

            <li class="divider"></li>

            <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>

            </li>

        </ul>

        <!-- /.dropdown-user -->

    </li>

    <!-- /.dropdown -->

</ul>

<!-- /.navbar-top-links -->