Hello <i>{{ $mail->receiver }}</i>,
 
<p>Pemberitahun Gaji Berkala:</p>
 
<div>
<p>Anda akan mengalami kenaikan gaji berkala pada,</p>

<p><b>Bulan:&nbsp;{{ $mail->demo_two }} </b></p>
<p><b>Tahun:&nbsp;{{ $mail->demo_one }} </b></p>

<p>Segera persiapkan berkas kenaikan gaji berkala anda.</p>
</div>
 
<br>
 
 
Terimakasih,
<br/>
<i>{{ $mail->sender }}</i>