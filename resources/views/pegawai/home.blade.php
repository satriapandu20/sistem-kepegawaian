@extends ('layout/master')
@section ('title','Pegawai')
@section ('content')

<div class="row">

    <div class="col-lg-12">
    <div class="page-header">

        <h3>Halaman Pegawai</h3>
        
     </div>
     </div>
</div>
        <div class="col-xs-4 pull-right">
        <form action="{{ url('pegawai/search')}}" method="POST" role="search">
    {{ csrf_field() }}
    <div class="input-group">
        <input type="text" class="form-control" name="query"
            placeholder="Cari.."> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>
       	</div>

<a href="{{ url('pegawai/create') }}"><button class="btn btn-success"><i class="fa fa-plus"></i> Tambah Pegawai</button></a>
<br>
<br>   
	

	<table class="table table-striped table-bordered table-hover">
	<thead>

	<tr>
		<th>#</th>
		<th>NIK</th>
		<th class="col-lg-5">Nama</th>
		<th>Aksi</th>

	</tr>
	</thead>

	<tbody>
    
	@foreach($pg as $pg)
    <tr>
        <td></td>
        <td class="text-uppercase">{{$pg->NIK}}</td>
        <td class="text-uppercase">{{$pg->nama}}</td>
        <td align="center">
        <a href="{{ url('pegawai/'.$pg->id_pegawai) }}"><button type="button" class="btn btn-info"><i class="fa fa-edit"></i></button></a>
                      <a href="#"><button type="button" name="button" class="btn btn-danger deleteUser" data-userid="{{$pg->id_pegawai}}"><i class="fa fa-trash"></i></button></a>

        </td>

    </tr>
    
    @endforeach



	</tbody>

	</table>
	</div>
</div>
</div>



<div id="applicantDeleteModal" class="modal modal-danger fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
             <form action="{{route('applicant_delete')}}" method="POST" class="remove-record-model">
               {{ method_field('delete') }}
               {{ csrf_field() }}

            <div class="modal-header"  style="background: #B22222;color: #fff;">
                
                <h4 class="modal-title text-center" id="custom-width-modalLabel">Hapus dari Daftar Pegawai ?</h4>
            </div>
            <div class="modal-body">
                <h5>Status pegawai akan menjadi <b>tidak aktif</b>, tekan tombol <b>Non Aktif</b> untuk melanjutkan.</h5>
                <input type="hidden" name="id_pegawai" id="app_id" value="">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger waves-effect remove-data-from-delete-form">Non Aktif</button>
            </div>

             </form>
        </div>
    </div>
</div>


<script type="text/javascript">

// function passID($id){
//     var delete_url="http://localhost/smecone/pegawai/"+id+"/non_aktif";
//     document.getElementById('delete_link').setAttribute('href' , 'FUCK');
//     $(".modal-footer #delete_link").setAttribute('href' , 'FUCK')
// }



</script>

<script>
$(document).on('click','.deleteUser',function(){
    var userID=$(this).attr('data-userid');
    $('#app_id').val(userID);

    $('#applicantDeleteModal').modal('show'); 
});
</script>


<!--  -->
@endsection