@extends('layout/master')
@section('title','Tambah Pegawai')
@section('content')

<div class="row">

    <div class="col-lg-12">

        <h4 class="page-header">Halaman Tambah Pegawai</h4>
     </div>
</div>


	<form class="form-horizontal row" action="{{ url('pegawai') }}" method="post" enctype="multipart/form-data">
	
	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Identitas

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">

			<div class="form-group">
				<label class="col-sm-3 control-label">Nama*</label>
				<div class="col-sm-6">
				<input type="text" name="nama" autocomplete="off" class="form-control text-uppercase">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">NIK*</label>
				<div class="col-sm-4">
				<input type="text" name="nik" autocomplete="off" class="form-control text-uppercase">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Kelamin*</label>
				<div class="col-sm-4">
				<div class="radio">
				  <label><input type="radio" name="jenis_kelamin" value="L">Laki-laki</label>
				  <label><input type="radio" name="jenis_kelamin" value="P">Perempuan</label>
				</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tempat Lahir*</label>
				<div class="col-sm-3">
				<input type="text" name="tempat_lahir" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Lahir*</label>

				<div class="col-sm-1">
				<input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control text-uppercase" autocomplete="off" maxlength="2" onkeypress="return hanyaAngka(event)">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_lahir" id="bulan_lahir">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" id="tahun_lahir" name="tahun_lahir" autocomplete="off" placeholder="Tahun" maxlength="4" onkeypress="return hanyaAngka(event)">
				</div>

			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Foto*</label>
				<div class="col-sm-4">
				<input type="file" name="foto" autocomplete="off" accept="image/jpeg,image/jpg,image/png,">
				</div>
			</div>

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Agama*</label>
	          <div class="col-lg-2">
	            <select ui-jq="chosen" class="form-control" name="agama">
	                <optgroup label="Pilih Agama">
	                    <option value="islam">Islam</option>
	                    <option value="kristen">Kristen</option>
	                    <option value="katolik">Katolik</option>
	                    <option value="hindu">Hindu</option>
	                    <option value="budha">Budha</option>
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Status*</label>
	          <div class="col-lg-3">
	            <select ui-jq="chosen" class="form-control" name="status_perkawinan" id="status_perkawinan" onchange="perkawinan()">
	                <optgroup label="Status">
	                    <option value="Belum Kawin">Belum Kawin</option>
	                    <option value="Kawin">Kawin</option>
	                    <option value="Cerai hidup">Cerai Hidup</option>
	                    <option value="Cerai mati">Cerai Mati</option>
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>


<!-- 			@if (count($errors) > 0)
    <div class="alert alert-danger">
     Upload Validation Error<br><br>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>	
      @endforeach
     </ul>
    </div>
   @endif -->
  <!--  @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   <div class="form-group">
   <img src="/smecone/public/images/{{ Session::get('path') }}" width="150" />
   </div>
   @endif -->

			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Ibu*</label>
				<div class="col-sm-4">
				<input type="text" name="nama_ibu_kandung" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Data Pribadi

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

			<div class="form-group">
				<label class="col-sm-3 control-label">Alamat Lengkap*</label>
				<div class="col-sm-4">
				<textarea class="form-control text-uppercase" rows="2" name="alamat"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">RT*</label>
				<div class="col-lg-1">
				<input type="text" name="rt" class="form-control text-uppercase" maxlength="2" onkeypress="return hanyaAngka(event)">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">RW*</label>
				<div class="col-lg-1">
				<input type="text" name="rw" class="form-control text-uppercase" maxlength="2" onkeypress="return hanyaAngka(event)">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kelurahan*</label>
				<div class="col-lg-3">
				<input type="text" name="kelurahan" class="form-control text-uppercase">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kecamatan*</label>
				<div class="col-lg-3">
				<input type="text" name="kecamatan" class="form-control text-uppercase">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kabupaten*</label>
				<div class="col-lg-3">
				<input type="text" name="kabupaten" class="form-control text-uppercase">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Kode Pos*</label>
				<div class="col-lg-2">
				<input type="text" name="kode_pos" class="form-control text-uppercase" maxlength="5" onkeypress="return hanyaAngka(event)">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Nomor Telepon*</label>
				<div class="col-sm-3">

				<div class="input-group">
				<span class="input-group-addon">62</span>
				<input type="text" name="nomor_telepon" class="form-control" autocomplete="off" maxlength="15" onkeypress="return hanyaAngka(event)">
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Email*</label>
				<div class="col-sm-4">
				<input type="email" name="email" class="form-control" autocomplete="off">
				</div>
			</div>
			
		
			<div class="form-group">
				<label class="col-sm-3 control-label">NPWP</label>
				<div class="col-sm-3">
				<input type="text" name="npwp" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Nama WP</label>
				<div class="col-sm-4">
				<input type="text" name="nama_wajib_pajak" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

	    </div>
		</div>
		</div>

	</div>
	</div>
	</div>

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Kepegawaian

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

        	<div class="form-group">
	          <label class="col-sm-3 control-label">Status Kepegawaian*</label>
	          <div class="col-sm-3">
	            <select ui-jq="chosen" class="form-control" name="status_kepegawaian" id="status_kepegawaian" onchange="kepegawaian()">
	                <optgroup label="Status Kepegawaian">
	                    <option value="PNS">PNS</option>
	                    <option value="CPNS">CPNS</option>
	                    <option value="GTT/PTT">GTT/PTT</option>
	                    </optgroup>
	            </select>
	          </div>
	        </div>


        	<div class="form-group"  id="kartu_pegawai">
				<label class="col-sm-3 control-label">Kartu Pegawai</label>
				<div class="col-sm-3">
				<input type="text" name="kartu_pegawai" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

	        <div class="form-group"  id="nuptk">
				<label class="col-sm-3 control-label">NUPTK</label>
				<div class="col-sm-3">
				<input type="text" name="nuptk" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

	        <div class="form-group"  id="nip">
				<label class="col-sm-3 control-label">NIP</label>
				<div class="col-sm-3">
				<input type="text" name="nip" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
	          <label class="col-sm-3 control-label">Jenis PTK*</label>
	          <div class="col-lg-3">
	            <select ui-jq="chosen" class="form-control" name="jenis_gtk" id="jenis_gtk">
	                <optgroup label="GTK">
	                    <option value="Guru BK">Guru BK</option>
	                    <option value="Guru Mapel">Guru Mapel</option>
	                    <option value="Guru TIK">Guru TIK</option>
	                    <option value="Kepala Sekolah">Kepala Sekolah</option>
	                    <option value="Laboran">Laboran</option>
	                    <option value="Penjaga Sekolah">Penjaga Sekolah</option>
	                    <option value="Pesuruh/OB">Pesuruh/OB</option>
	                    <option value="Keamanan">Keamanan</option>
	                    <option value="Tenaga Administrasi">Tenaga Administrasi</option>
	                    <option value="Tenaga Perpustakaan">Tenaga Perpustakaan</option>
	                    <option value="Tukang Kebun">Tukang Kebun</option>
	                    </optgroup>
	            </select>
	          </div>
	        </div>

			<div class="form-group">
				<label class="col-sm-3 control-label">SK CPNS</label>
				<div class="col-sm-3">
				<input type="text" name="sk_cpns" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>
		

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT CPNS</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt_cpns" class="form-control text-uppercase" autocomplete="off" maxlength="2" onkeypress="return hanyaAngka(event)">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt_cpns">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_tmt_cpns" autocomplete="off" placeholder="Tahun" maxlength="4" onkeypress="return hanyaAngka(event)">
				</div>

			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT SK Pengangkatan</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt_sk_pengangkatan" class="form-control text-uppercase" autocomplete="off" maxlength="2" onkeypress="return hanyaAngka(event)">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt_sk_pengangkatan">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_tmt_sk_pengangkatan" autocomplete="off" placeholder="Tahun" maxlength="4" onkeypress="return hanyaAngka(event)">
				</div>

			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Sumber Gaji*</label>
				<div class="col-sm-3">
				<input type="text" name="sumber_gaji" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>
			

			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja*</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja" class="form-control text-uppercase" autocomplete="off" id="masa_kerja" readonly>
				<span class="input-group-addon">Tahun</span>
				</div>

				</div>
			</div>

				<div class="form-group">
				<label class="col-sm-3 control-label">Tahun Pensiun*</label>
				<div class="col-lg-2">
				<input type="text" name="tahun_pensiun" class="form-control text-uppercase" autocomplete="off" id="tahun_pensiun" readonly>
				</div>
			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>

	<!-- <div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Data Pangkat Golongan

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Pangkat Golongan</label>
	          <div class="col-lg-3">
	            <select ui-jq="chosen" class="form-control" name="pangkat_golongan">
	                <optgroup label="Pangkat Golongan">
	                    <option value="III/a">III/a</option>
	                    <option value="III/b">III/b</option>
	                    <option value="III/c">III/c</option>
	                    <option value="III/d">III/d</option>
	                    <option value="IV/a">IV/a</option>
	                    <option value="IV/b">IV/b</option>
	                    <option value="IV/c">IV/c</option>
	                    <option value="IV/d">IV/d</option>
	                    <option value="IV/e">IV/e</option>

	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

	        <div class="form-group">
				<label class="col-sm-3 control-label">TMT Pangkat Golongan</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt_pangkat_golongan" class="form-control text-uppercase" autocomplete="off">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt_pangkat_golongan">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_tmt_pangkat_golongan" autocomplete="off" placeholder="Tahun">
				</div>

			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">SK Pengangkatan</label>
				<div class="col-sm-3">
				<input type="text" name="sk_pengangkatan" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT Pengangkatan</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt_pengangkatan" class="form-control text-uppercase" autocomplete="off">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt_pengangkatan">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_tmt_pengangkatan" autocomplete="off" placeholder="Tahun">
				</div>

			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Lembaga Pengangkat</label>
				<div class="col-sm-3">
				<input type="text" name="lembaga_pengangkatan" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>
 -->
	<div class="row" id="pasangan">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Data Pasangan

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

        	<div class="form-group">
				<label class="col-sm-3 control-label">Nama Pasangan</label>
				<div class="col-sm-4">
				<input type="text" name="nama_pasangan" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Pekerjaan Pasangan</label>
				<div class="col-sm-3">
				<input type="text" name="pekerjaan_pasangan" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kartu Pasangan</label>
				<div class="col-sm-3">
				<input type="text" name="kartu_pasangan" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

		
		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Tambah</button>
		
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
	
	</form>


<script type="text/javascript">

    

    var $tl=$("#tanggal_lahir,#bulan_lahir,#tahun_lahir,#jenis_gtk");
    
    $tl.on('change',function(){
    	var tanggal=$('#tanggal_lahir').val();
    	var bulan=$('#bulan_lahir').val();
    	var tahun=$('#tahun_lahir').val();
    	var tgl= tahun+bulan+tanggal;
    	var jenis_gtk=$('#jenis_gtk').val();

    	var myyear= tgl.slice(0,4);
    	var year=new Date().getFullYear();
    	var age=year-myyear;
    	
    	var max=0;
    	if(jenis_gtk=="Guru BK"||jenis_gtk=="Guru Mapel"||jenis_gtk=="Guru TIK"||jenis_gtk=="Kepala Sekolah")
    	{
    		max=60;
    	}else{
    		max=58;
    	}


    	var pensiun= year+(max-age);
    	$('#tahun_pensiun').val(pensiun);
    	$('#masa_kerja').val(max-age);

    });


    document.getElementById("pasangan").style.display = "none";


    function perkawinan()
    {

	   //
	   var status_perkawinan=$('#status_perkawinan').val();
	   
	    if(status_perkawinan!="Belum Kawin"){

	    	
	    	document.getElementById("pasangan").style.display = "block";

	    }else if(status_perkawinan=="Belum Kawin"){

	    	document.getElementById("pasangan").style.display = "none";
	    }

	}

	function kepegawaian()
	{
	 	var status_kepegawaian=$('#status_kepegawaian').val();

	 	if(status_kepegawaian=='GTT/PTT')
	 	{
	 		document.getElementById("nip").style.display = "none";
	 		document.getElementById("kartu_pegawai").style.display = "none";
	 		document.getElementById("nuptk").style.display = "none";

	 	}
	 	else
	 	{
	 		document.getElementById("nip").style.display="block";
	 		document.getElementById("kartu_pegawai").style.display = "block";
	 		document.getElementById("nuptk").style.display = "block";
	 	}
	}

	

</script>  

<script>
	
	$(document).ready(function () {
   	//$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    //$('.expander-drop').slideUp(200);
	});	

</script>


@endsection
