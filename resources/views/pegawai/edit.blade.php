@extends('layout/master')
@section('title','Halaman Pegawai')
@section('content')
<div class="row">

    <div class="col-lg-12">
    <div  class="page-header">

        <h4 class="text-uppercase">Halaman {{$pg->nama}} <a href="{{ url('pegawai/'.$pg->id_pegawai.'/all')}}"><button class="btn btn-outline btn-info pull-right"><i class="fa fa-print"></i> Cetak</button></a></h4>
        <?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>
        	
    </div>
    </div>
    </div>

        	

	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai) }}" method="post" enctype="multipart/form-data">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Identitas

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">
        	

			<div class="form-group">
				<label class="col-sm-3 control-label">Nama</label>
				<div class="col-sm-4">
				<input type="text" name="nama" autocomplete="off" class="form-control text-uppercase" value="{{$pg->nama}}" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">NIK</label>
				<div class="col-sm-4">
				<input type="text" name="nik" autocomplete="off" class="form-control text-uppercase" value="{{$pg->NIK}}" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Kelamin</label>
				<div class="col-sm-4">
				<div class="radio">
					<?php 
				$jk_l="";
				$jk_p="";
				if($pg->jenis_kelamin=="L"){
					$jk_l="checked";
				}else{
					$jk_p="checked";
				}
			?>
			  <label><input type="radio" name="jenis_kelamin" value="L" {{ $jk_l }}>Laki-laki</label>
			  <label><input type="radio" name="jenis_kelamin" value="P" {{ $jk_p }}>Perempuan</label>
				</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tempat Lahir</label>
				<div class="col-sm-3">
				<input type="text" name="tempat_lahir" class="form-control text-uppercase" autocomplete="off" value="{{$pg->tempat_lahir}}" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Lahir</label>
				<div class="col-sm-1">
				<input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control text-uppercase" autocomplete="off" value="{{ $tanggal_lahir['d'] }}">
				</div>

				<?php 

				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tanggal_lahir['m']=="01")
				{
					$Januari="selected";
				}
				else if($tanggal_lahir['m']=="02")
				{
					$Februari="selected";
				}
				else if($tanggal_lahir['m']=="03")
				{
					$Maret="selected";
				}
				else if($tanggal_lahir['m']=="04")
				{
					$April="selected";
				}
				else if($tanggal_lahir['m']=="05")
				{
					$Mei="selected";
				}
				else if($tanggal_lahir['m']=="06")
				{
					$Juni="selected";
				}
				else if($tanggal_lahir['m']=="07")
				{
					$Juli="selected";
				}
				else if($tanggal_lahir['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tanggal_lahir['m']=="09")
				{
					$September="selected";
				}
				else if($tanggal_lahir['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tanggal_lahir['m']=="11")
				{
					$November="selected";
				}
				else if($tanggal_lahir['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_lahir" id="bulan_lahir">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" id="tahun_lahir" name="tahun_lahir" autocomplete="off" value="{{ $tanggal_lahir['y']}}">
				</div>

			</div>

			<div class="form-group">
   			
				<label class="col-sm-3 control-label">Foto</label>
				
				<div class="col-sm-4">
   				<img src="/smecone/public/images/{{ $pg->foto }}" width="150" class="img-responsive"/>
   				<br>
				<input type="file" name="foto" class="form-control text-uppercase" autocomplete="off">
				</div>
   			</div>

   			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Agama</label>
	          <div class="col-lg-3">
		        <?php
	          	$islam="";
	          	$kristen="";
	          	$katolik="";
	          	$hindu="";
	          	$budha="";

	          	if($pg->agama=="islam"){
	          		$islam="selected";
	          	}else if($pg->agama=="kristen"){
	          		$kristen="selected";
	          	}else if($pg->agama=="katolik"){
	          		$katolik="selected";
	          	}else if($pg->agama=="hindu"){
	          		$hindu="selected";
	          	}else if($pg->agama=="budha"){
	          		$budha="selected";
	          	}
	          ?>
	            <select ui-jq="chosen" class="form-control text-uppercase" name="agama">
	                <optgroup label="Pilih Agama">
	                    <option value="islam" {{ $islam }} >Islam</option>
	                    <option value="kristen" {{ $kristen }} >Kristen</option>
	                    <option value="katolik" {{ $katolik }} >Katolik</option>
	                    <option value="hindu" {{ $hindu }} >Hindu</option>
	                    <option value="budha" {{ $budha }} >Budha</option>
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>


			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Status</label>
	          <div class="col-lg-3">
	          <?php
	          $kawin="";
	          $blm="";
	          $chidup="";
	          $cmati="";

	          if($pg->status_perkawinan=="Kawin")
	          {
	          	$kawin="selected";
	          }
	          else if($pg->status_perkawinan=="Belum Kawin")
	          {
	          	$blm="selected";
	          }
	          else if($pg->status_perkawinan=="Cerai Hidup")
	          {
	          	$chidup="selected";
	          }
	          else if($pg->status_perkawinan=="Cerai Mati"){
	          	$cmati="selected";
	          }

	          

	          ?>
	            <select ui-jq="chosen" class="form-control text-uppercase" name="status_perkawinan" id="status_perkawinan" onchange="perkawinan()">
	                <optgroup label="Status">
	                    <option value="Belum Kawin" {{ $blm }}>Belum Kawin</option>
	                    <option value="Kawin" {{$kawin}}>Kawin</option>
	                    <option value="Cerai hidup" {{$chidup}}>Cerai Hidup</option>
	                    <option value="Cerai mati" {{$cmati}}>Cerai Mati</option>
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Ibu</label>
				<div class="col-sm-4">
				<input type="text" name="nama_ibu_kandung" class="form-control text-uppercase" autocomplete="off" value="{{$pg->nama_ibu_kandung}}" >
				</div>
			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Data Pribadi

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

			<div class="form-group">
				<label class="col-sm-3 control-label">Alamat Lengkap</label>
				<div class="col-sm-4">
				<textarea class="form-control text-uppercase" rows="2" name="alamat">{{$pg->alamat}}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">RT</label>
				<div class="col-lg-1">
				<input type="text" name="rt" class="form-control text-uppercase" value="{{$pg->RT}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">RW</label>
				<div class="col-lg-1">
				<input type="text" name="rw" class="form-control text-uppercase" value="{{$pg->RW}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kelurahan</label>
				<div class="col-lg-3">
				<input type="text" name="kelurahan" class="form-control text-uppercase" value="{{$pg->kelurahan}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kecamatan</label>
				<div class="col-lg-3">
				<input type="text" name="kecamatan" class="form-control text-uppercase" value="{{$pg->kecamatan}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kabupaten</label>
				<div class="col-lg-3">
				<input type="text" name="kabupaten" class="form-control text-uppercase" value="{{$pg->kabupaten}}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Kode Pos</label>
				<div class="col-lg-2">
				<input type="text" name="kode_pos" class="form-control text-uppercase" value="{{$pg->kode_pos}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Nomor Telepon</label>
				<div class="col-sm-4">
				<input type="text" name="nomor_telepon" class="form-control" autocomplete="off" value="{{$pg->nomor_telepon}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Email</label>
				<div class="col-sm-4">
				<input type="email" name="email" class="form-control" autocomplete="off" value="{{$pg->email}}">
				</div>
			</div>
			
		
			<div class="form-group">
				<label class="col-sm-3 control-label">NPWP</label>
				<div class="col-sm-3">
				<input type="text" name="npwp" class="form-control text-uppercase" autocomplete="off" value="{{$pg->NPWP}}" >

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Nama WP</label>
				<div class="col-sm-4">
				<input type="text" name="nama_wajib_pajak" class="form-control text-uppercase" autocomplete="off" value="{{$pg->nama_wajib_pajak}}" >
				</div>
			</div>

	    </div>
		</div>
		</div>

	</div>
	</div>
	</div>

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Kepegawaian

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">


        	<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Status Kepegawaian</label>
	          <div class="col-lg-3">
		        <?php
	          	$PNS="";
	          	$CPNS="";
	          	$GTT="";

	          	if($pg->status_kepegawaian=="PNS"){
	          		$PNS="selected";
	          	}else if($pg->status_kepegawaian=="CPNS"){
	          		$CPNS="selected";
	          	}else if($pg->status_kepegawaian=="GTT/PTT"){
	          		$GTT="selected";
	          	}

	          ?>
	            <select ui-jq="chosen" class="form-control text-uppercase" name="status_kepegawaian" onchange="kepegawaian()" id="status_kepegawaian">
	                <optgroup label="Status Kepegawaian">
	                    <option value="PNS" {{ $PNS }} >PNS</option>
	                    <option value="CPNS" {{ $CPNS }} >CPNS</option>
	                    <option value="GTT/PTT" {{ $GTT }} >GTT/PTT</option>
	                    </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Jenis GTK</label>
	          <div class="col-lg-3">
		        <?php
	          	$bk="";
	          	$mapel="";
	          	$tik="";
	          	$kepsek="";
	          	$laboran="";
	          	$penjaga="";$pesuruh="";$keamanan="";$tadmin="";$tperpus="";$tkebun="";
	          	if($pg->jenis_gtk=="Guru BK"){
	          		$bk="selected";
	          	}else if($pg->jenis_gtk=="Guru Mapel"){
	          		$mapel="selected";
	          	}else if($pg->jenis_gtk=="Guru TIK"){
	          		$tik="selected";
	          	}else if($pg->jenis_gtk=="Kepala Sekolah"){
	          		$kepsek="selected";
	          	}else if($pg->jenis_gtk=="Laboran"){
	          		$laboran="selected";
	          	}else if($pg->jenis_gtk=="Penjaga Sekolah"){
	          		$penjaga="selected";
	          	}else if($pg->jenis_gtk=="Pesuruh/OB"){
	          		$pesuruh="selected";
	          	}else if($pg->jenis_gtk=="Keamanan"){
	          		$keamanan="selected";
	          	}else if($pg->jenis_gtk=="Tenaga Administrasi"){
	          		$tadmin="selected";
	          	}else if($pg->jenis_gtk=="Tenaga Perpustakaan"){
	          		$tperpus="selected";
	          	}else if($pg->jenis_gtk=="Tukang Kebun"){
	          		$tkebun="selected";
	          	}

	          ?>
	            <select ui-jq="chosen" class="form-control text-uppercase" name="jenis_gtk" id="jenis_gtk">
	                <optgroup label="GTK">
	                    <option value="Guru BK" {{ $bk }} >Guru BK</option>
	                    <option value="Guru Mapel" {{ $mapel }} >Guru Mapel</option>
	                    <option value="Guru TIK" {{ $tik }} >Guru TIK</option>
	                    <option value="Kepala Sekolah" {{ $kepsek }} >Kepala Sekolah</option>
	                    <option value="Laboran" {{ $laboran }}>Laboran</option>
	                    <option value="Penjaga Sekolah" {{ $penjaga }}>Penjaga Sekolah</option>
	                    <option value="Pesuruh/OB" {{ $pesuruh }}>Pesuruh/OB</option>
	                    <option value="Keamanan" {{ $keamanan }}>Keamanan</option>
	                    <option value="Tenaga Administrasi" {{ $tadmin }}>Tenaga Administrasi</option>
	                    <option value="Tenaga Perpustakaan" {{ $tperpus }}>Tenaga Perpustakaan</option>
	                    <option value="Tukang Kebun" {{ $tkebun }}>Tukang Kebun</option>
	                    </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

	        <div class="form-group" id="nuptk">
				<label class="col-sm-3 control-label">NUPTK</label>
				<div class="col-sm-3">
				<input type="text" name="nuptk" class="form-control text-uppercase" autocomplete="off" value="{{$pg->NUPTK}}">
				</div>
			</div>

	        <div class="form-group" id="nip">
				<label class="col-sm-3 control-label">NIP</label>
				<div class="col-sm-3">
				<input type="text" name="nip" class="form-control text-uppercase" autocomplete="off" value="{{$pg->NIP}}">
				</div>
			</div>
		

        	<div class="form-group" id="kartu_pegawai">
				<label class="col-sm-3 control-label">Kartu Pegawai</label>
				<div class="col-sm-3">
				<input type="text" name="kartu_pegawai" class="form-control text-uppercase" autocomplete="off" value="{{$pg->kartu_pegawai}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">SK CPNS</label>
				<div class="col-sm-3">
				<input type="text" name="sk_cpns" class="form-control text-uppercase" autocomplete="off" value="{{$pg->SK_CPNS}}" >
				</div>
			</div>
		

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT CPNS</label>
				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt_cpns" class="form-control text-uppercase" autocomplete="off" value="{{ $tmt_cpns['d'] }}">
				</div>

				<?php 

				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tmt_cpns['m']=="01")
				{
					$Januari="selected";
				}
				else if($tmt_cpns['m']=="02")
				{
					$Februari="selected";
				}
				else if($tmt_cpns['m']=="03")
				{
					$Maret="selected";
				}
				else if($tmt_cpns['m']=="04")
				{
					$April="selected";
				}
				else if($tmt_cpns['m']=="05")
				{
					$Mei="selected";
				}
				else if($tmt_cpns['m']=="06")
				{
					$Juni="selected";
				}
				else if($tmt_cpns['m']=="07")
				{
					$Juli="selected";
				}
				else if($tmt_cpns['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tmt_cpns['m']=="09")
				{
					$September="selected";
				}
				else if($tmt_cpns['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tmt_cpns['m']=="11")
				{
					$November="selected";
				}
				else if($tmt_cpns['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt_cpns">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_tmt_cpns" autocomplete="off" value="{{ $tmt_cpns['y']}}">
				</div>

			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">TMT SK Pengangkatan</label>
				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt_sk_pengangkatan" class="form-control text-uppercase" autocomplete="off" value="{{ $tmt_sk_pengangkatan['d'] }}">
				</div>

				<?php 

				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tmt_sk_pengangkatan['m']=="01")
				{
					$Januari="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="02")
				{
					$Februari="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="03")
				{
					$Maret="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="04")
				{
					$April="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="05")
				{
					$Mei="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="06")
				{
					$Juni="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="07")
				{
					$Juli="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="09")
				{
					$September="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="11")
				{
					$November="selected";
				}
				else if($tmt_sk_pengangkatan['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt_sk_pengangkatan">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_tmt_sk_pengangkatan" autocomplete="off" value="{{ $tmt_sk_pengangkatan['y']}}">
				</div>

			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Sumber Gaji</label>
				<div class="col-sm-3">
				<input type="text" name="sumber_gaji" class="form-control text-uppercase" autocomplete="off" value="{{$pg->sumber_gaji}}">
				</div>
			</div>


			<!-- <div class="form-group"><td>
	          <label class="col-sm-3 control-label">Pangkat Golongan</label>
	          <div class="col-lg-3">
		        <?php
	          	$IIIa="";
	          	$IIIb="";
	          	$IIIc="";
	          	$IIId="";
	          	$IVa="";
	          	$IVb="";
	          	$IVc="";
	          	$IVd="";
	          	$IVe="";

	          	if($pg->pangkat_golongan=="III/a"){
	          		$IIIa="selected";
	          	}else if($pg->pangkat_golongan=="III/b"){
	          		$IIIb="selected";
	          	}else if($pg->pangkat_golongan=="III/c"){
	          		$IIIc="selected";
	          	}else if($pg->pangkat_golongan=="III/d"){
	          		$IIId="selected";
	          	}else if($pg->pangkat_golongan=="IV/a"){
	          		$IVa="selected";
	          	}else if($pg->pangkat_golongan=="IV/b"){
	          		$IVb="selected";
	          	}else if($pg->pangkat_golongan=="IV/c"){
	          		$IVc="selected";
	          	}else if($pg->pangkat_golongan=="IV/d"){
	          		$IVd="selected";
	          	}else if($pg->pangkat_golongan=="IV/e"){
	          		$IVe="selected";
	          	}

	          ?>
	            <select ui-jq="chosen" class="form-control" name="pangkat_golongan">
	                <optgroup label="Pangkat Golongan">
	                    <option value="III/a" {{ $IIIa }} >III/a</option>
	                    <option value="III/b" {{ $IIIb }} >III/b</option>
	                    <option value="III/c" {{ $IIIc }} >III/c</option>
	                    <option value="III/d" {{ $IIId }} >III/d</option>
	                    <option value="IV/a" {{ $IVa }} >IV/a</option>
	                    <option value="IV/b" {{ $IVb }} >IV/b</option>
	                    <option value="IV/c" {{ $IVc }} >IV/c</option>
	                    <option value="IV/d" {{ $IVd }} >IV/d</option>
	                    <option value="IV/e" {{ $IVe }} >IV/e</option>

	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

	        <div class="form-group">
				<label class="col-sm-3 control-label">TMT Pangkat Gologan</label>
				<div class="col-sm-3">
				<input type="text" name="tmt_pangkat_golongan" class="form-control text-uppercase date" autocomplete="off" value="{{$pg->TMT_pangkat_golongan}}">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">SK Pengangkatan</label>
				<div class="col-sm-3">
				<input type="text" name="sk_pengangkatan" class="form-control text-uppercase" autocomplete="off" value="{{$pg->SK_pengangkatan}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT Pengangkatan</label>
				<div class="col-sm-3">
				<input type="text" name="tmt_pengangkatan" class="form-control text-uppercase date" autocomplete="off" value="{{$pg->TMT_pengangkatan}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Lembaga Pengangkat</label>
				<div class="col-sm-3">
				<input type="text" name="lembaga_pengangkatan" class="form-control text-uppercase" autocomplete="off" value="{{$pg->lembaga_pengangkatan}}">
				</div>
			</div>
 -->

			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja" class="form-control text-uppercase" autocomplete="off" id="masa_kerja" value="{{$pg->masa_kerja}}" readonly>
				<span class="input-group-addon">Tahun</span>
				</div>

				</div>
			</div>

				<div class="form-group">
				<label class="col-sm-3 control-label">Tahun Pensiun</label>
				<div class="col-lg-2">

				
				<input type="text" name="tahun_pensiun" class="form-control text-uppercase" autocomplete="off" id="tahun_pensiun" value="{{$pg->tahun_pensiun}}" readonly>
				

				</div>
			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>

	<div class="row" id="pasangan">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Data Pasangan

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

        	 <div class="form-group">
				<label class="col-sm-3 control-label">Nama Pasangan</label>
				<div class="col-sm-4">
				<input type="text" name="nama_pasangan" class="form-control text-uppercase" autocomplete="off" value="{{ $pg->nama_pasangan }}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Pekerjaan Pasangan</label>
				<div class="col-sm-3">
				<input type="text" name="pekerjaan_pasangan" class="form-control text-uppercase" autocomplete="off" value="{{$pg->pekerjaan_pasangan}}" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Kartu Pasangan</label>
				<div class="col-sm-3">
				<input type="text" name="kartu_pasangan" class="form-control text-uppercase" autocomplete="off" value="{{$pg->kartu_pasangan}}">
				</div>
			</div>

			<!-- <div class="form-group">
				<label class="col-sm-3 control-label">Kompetensi</label>
				<div class="col-sm-3">
				<input type="text" name="kompetensi" class="form-control text-uppercase" autocomplete="off" value="{{$pg->kompetensi}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Pendidikan Terakhir</label>
				<div class="col-sm-3">
				<input type="text" name="pendidikan_terakhir" class="form-control text-uppercase" autocomplete="off" value="{{$pg->pendidikan_terakhir}}">
				</div>
			</div> -->

			

		
		</div>
		</div>
		</div>

	</div>
	</div>
	</div>
		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>
		</div>

		<br>
		<br>


		{{ csrf_field() }}
	<input type="hidden" name="_method" value="PUT">
	</form>


<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });
    var $tl=$("#tanggal_lahir,#bulan_lahir,#tahun_lahir,#jenis_gtk");
    
    $tl.on('change',function(){
    	var tanggal=$('#tanggal_lahir').val();
    	var bulan=$('#bulan_lahir').val();
    	var tahun=$('#tahun_lahir').val();
    	var tgl= tahun+bulan+tanggal;
    	var jenis_gtk=$('#jenis_gtk').val();

    	var myyear= tgl.slice(0,4);
    	var year=new Date().getFullYear();
    	var age=year-myyear;
    	
    	var max=0;
    	if(jenis_gtk=="Guru BK"||jenis_gtk=="Guru Mapel"||jenis_gtk=="Guru TIK"||jenis_gtk=="Kepala Sekolah")
    	{
    		max=60;
    	}else{
    		max=58;
    	}


    	var pensiun= year+(max-age);
    	$('#tahun_pensiun').val(pensiun);
    	$('#masa_kerja').val(max-age);

    });

    if($('#status_perkawinan').val()=="Belum Kawin"){
    document.getElementById("pasangan").style.display = "none";
	}


    function perkawinan()
    {

	   //
	   var status_perkawinan=$('#status_perkawinan').val();
	   
	    if(status_perkawinan!="Belum Kawin"){

	    	
	    	document.getElementById("pasangan").style.display = "block";

	    }else if(status_perkawinan=="Belum Kawin"){

	    	document.getElementById("pasangan").style.display = "none";
	    }

	}

	if($('#status_kepegawaian').val()=="GTT/PTT"){
		
			document.getElementById("nip").style.display = "none";
	 		document.getElementById("kartu_pegawai").style.display = "none";
	 		document.getElementById("nuptk").style.display = "none";

	}

	function kepegawaian()
	 {
	 	var status_kepegawaian=$('#status_kepegawaian').val();

	 	if(status_kepegawaian=='GTT/PTT')
	 	{
	 		document.getElementById("nip").style.display = "none";
	 		document.getElementById("kartu_pegawai").style.display = "none";
	 		document.getElementById("nuptk").style.display = "none";

	 	}
	 	else
	 	{
	 		document.getElementById("nip").style.display="block";
	 		document.getElementById("kartu_pegawai").style.display = "block";
	 		document.getElementById("nuptk").style.display = "block";
	 	}
	 }

</script>  

<script>
	
	$(document).ready(function () {
   	$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    $('.expander-drop').slideUp(200);
	});	

</script>


@endsection
