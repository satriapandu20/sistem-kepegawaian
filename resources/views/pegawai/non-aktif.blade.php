@extends ('layout/master')
@section ('title','Pegawai')
@section ('content')

<div class="row">

    <div class="col-lg-12">
    <div class="page-header">

        <h3>Halaman Pegawai Tidak Aktif</h3>
        
     </div>
     </div>
</div>
        <div class="col-xs-4 pull-right">
        	<input type="text" class="form-control" placeholder="Cari...">
            
       	</div>

<br>
<br>
<br>   
	

	<table class="table table-striped table-bordered table-hover">
	<thead>

	<tr>
		<th>#</th>
		<th>NIK</th>
		<th class="col-lg-5">Nama</th>
		<th>Aksi</th>

	</tr>
	</thead>

	<tbody>

	@foreach($pgs as $pg)
	<tr>
		<td></td>
		<td class="text-uppercase">{{$pg->NIK}}</td>
		<td class="text-uppercase">{{$pg->nama}}</td>
		<td align="center">
		<a href="{{ url('pegawai/'.$pg->id_pegawai) }}"><button type="button" class="btn btn-info"><i class="fa fa-edit"></i></button></a>
                      <a href="#"><button type="button" name="button" class="btn btn-danger deleteUser" data-userid="{{$pg->id_pegawai}}"><i class="fa fa-trash"></i></button></a>

		</td>

	</tr>

	@endforeach

	</tbody>

	</table>
	</div>
</div>
</div>

<!-- Modal -->
 <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header" style="background: #B22222;color: #fff;">
                 
                 <h4 class="modal-title" id="myModalLabel">Hapus dari daftar pegawai ? </h4>
             </div>
             <div class="modal-body">
                    Status pegawai akan menjadi <b>tidak aktif</b>, tekan tombol <b>Non Aktif</b> untuk melanjutkan.
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                 <a href="#" id="delete_link"><button type="button" class="btn btn-danger">Non Aktif</button></a>
             </div>
         </div>
                                    <!-- /.modal-content -->
     </div>
                                <!-- /.modal-dialog -->
</div>
                            <!-- /.modal -->

<div id="applicantDeleteModal" class="modal modal-danger fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
             <form action="{{route('applicant_return')}}" method="POST" class="remove-record-model">
               {{ method_field('delete') }}
               {{ csrf_field() }}

            <div class="modal-header"  style="background:   #2E8B57;color: #fff;">
                
                <h4 class="modal-title text-center" id="custom-width-modalLabel">Aktifkan Pegawai ?</h4>
            </div>
            <div class="modal-body">
                <h5>Status pegawai akan kembali menjadi <b>aktif</b>, tekan tombol <b>Aktif</b> untuk melanjutkan.</h5>
                <input type="hidden" name="id_pegawai" id="app_id" value="">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect remove-data-from-delete-form">Aktifan</button>
            </div>

             </form>
        </div>
    </div>
</div>


<script type="text/javascript">

// function passID($id){
//     var delete_url="http://localhost/smecone/pegawai/"+id+"/non_aktif";
//     document.getElementById('delete_link').setAttribute('href' , 'FUCK');
//     $(".modal-footer #delete_link").setAttribute('href' , 'FUCK')
// }



</script>

<script>
$(document).on('click','.deleteUser',function(){
    var userID=$(this).attr('data-userid');
    $('#app_id').val(userID);

    $('#applicantDeleteModal').modal('show'); 
});
</script>
@endsection