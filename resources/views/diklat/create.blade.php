@extends('layout/master')
@section('title','Halaman Diklat')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div  class="page-header">

        <h4 class="text-uppercase">Halaman Tambah Diklat</h4>
        
		<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>
    </div>
    </div>
</div>


	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Diklat

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">

        	
			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Diklat</label>
				<div class="col-sm-4">
				<input type="text" name="jenis_diklat" autocomplete="off" class="form-control text-uppercase" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Diklat</label>
				<div class="col-sm-4">
				<input type="text" name="nama_diklat" autocomplete="off" class="form-control text-uppercase" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Penyelenggara</label>
				<div class="col-sm-4">
				<input type="text" name="penyelenggara" autocomplete="off" class="form-control text-uppercase" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tahun</label>
				<div class="col-sm-1">
				<input type="text" name="tahun" autocomplete="off" class="form-control text-uppercase" maxlength="4" onkeypress="return hanyaAngka(event)">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Peran</label>
				<div class="col-sm-3">
				<input type="text" name="peran" class="form-control text-uppercase" autocomplete="off" >
				</div>
			</div>

			

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Tambah</button>
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
	
	</form>





<script>
	
	$(document).ready(function () {
   	$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    $('.expander-drop').slideUp(200);
	});	

</script>


@endsection
