@extends('layout/master')
@section('title','Tambah Riwayat Pendidikan')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div class="page-header">
        <h4 class="text-uppercase">Riwayat Pendidikan {{ $pg->nama }}</h4>
        
		<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>
		
	</div>
    </div>


</div>
<br>

<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan')}}" method="post">

	<div class="row">
	<div class="col-lg-10">
	<div class="panel panel-default">

		<div class="panel-heading">
		<div class="expander">
			<i class="fa fa-angle-down pull-right"></i>

			Riwayat Pendidikan

		</div>
		</div>

		<div class="expander-drop">
		<div class="panel-body">
		<div class="row">

			<div class="form-group">
			<label class="col-sm-3 control-label">Jenjang*</label>

				<div class="col-sm-3">
				<select ui-jq="chosen" class="form-control" name="jenjang" id="jenjang" onchange="jenjang()">
				
					<optgroup label="Pilih Jenjang">
						<option value="SD/sederajat">SD/sederajat</option>
						<option value="SMP/sederajat">SMP/sederajat</option>
						<option value="SMA/sederajat">SMA/sederajat</option>
						<option value="D1">D1</option>
						<option value="D2">D2</option>
						<option value="D3">D3</option>
						<option value="D4">D4</option>
						<option value="S1">S1</option>
						<option value="S2">S2</option>
						<option value="S3">S3</option>
						
					</optgroup>
				</select>
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Nama Sekolah*</label>

				<div class="col-sm-4">
					<input type="text" class="form-control text-uppercase" name="nama_sekolah" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Bidang Studi</label>

				<div class="col-sm-4">
				<input type="text" name="bidang_studi" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			
			<div class="form-group">
			<label class="col-sm-3 control-label">Fakultas</label>

				<div class="col-sm-3">
					<input type="text" class="form-control text-uppercase" name="fakultas" autocomplete="off">
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Kependidikan</label>

				<div class="col-sm-3">
				<div class="radio">
					<label><input type=radio name="kependidikan" value="ya">Ya</label>
					<label><input type=radio name="kependidikan" value="tidak">Tidak</label>
				</div>
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Tahun Masuk*</label>

				<div class="col-sm-1">
					<input type="text" name="tahun_masuk" class="form-control text-uppercase" autocomplete="off" maxlength="4" onkeypress="return hanyaAngka(event)" > 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Tahun Lulus*</label>

				<div class="col-sm-1">
					<input type="text" name="tahun_lulus" class="form-control text-uppercase" autocomplete="off" maxlength="4" onkeypress="return hanyaAngka(event)"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">NIM/NIS*</label>

				<div class="col-sm-2">
					<input type="text" name="NIM" class="form-control" autocomplete="off"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Status*</label>

				<div class="col-sm-3">
				<div class="radio">
					<label><input type="radio" name="status" value="Masih Bersekolah">Masih Bersekolah</label>
					<label><input type="radio" name="status" value="Lulus">Lulus</label>
				</div>
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Semester*</label>

				<div class="col-sm-1">
					<input type="text" name="semester" class="form-control" autocomplete="off"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">IPK</label>

				<div class="col-sm-2">
					<input type="text" name="IPK" class="form-control" autocomplete="off"> 
				</div>

			</div>

			<div class="form-group" id="PT">
			<label class="col-sm-3 control-label">Gelar</label>

				<div class="col-sm-3">
					<input type="text" class="form-control" name="gelar" autocomplete="off">
				</div>

			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>

	<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Tambah</button>
		</div>
		</div>

	<br>
	<br> 

	{{ csrf_field() }}

</form>

<script type="text/javascript">

$(document).ready(function(){
	$('.expander-drop').slideDown(200);
	$('.expander').click(function(){

		$(this).parent().next().slideToggle(200);
	});
	//$('.expander-drop').slideUp(200);


});


	

	function jenjang()
    {
    	document.getElementById("PT").style.display = "none";
	   //
	   // var jenjang=$('#jenjang').val();
	   
	   //  if(jenjang!='SD/sederajat'||jenjang!='SMP/sederajat'||jenjang!='SMA/sederajat'){

	    	
	   //  	document.getElementById("PT").style.display = "block";

	   //  }else{

	   //  	document.getElementById("PT").style.display = "none";
	   //  }

	}

</script>

@endsection
