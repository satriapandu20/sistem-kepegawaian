@extends('layout/master')
@section('title','Edit Riwayat Pendidikan')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div class="page-header">
        <h4 class="text-uppercase">Edit Riwayat Pendidikan {{ $pg->nama }}</h4>
        
		<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>
		
	</div>
    </div>


</div>
<br>

<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan/'.$rwp->id_riwayat_pendidikan)}}" method="post">

	<div class="row">
	<div class="col-lg-10">
	<div class="panel panel-default">

		<div class="panel-heading">
		<div class="expander">
			<i class="fa fa-angle-down pull-right"></i>

			Riwayat Pendidikan

		</div>
		</div>

		<div class="expander-drop">
		<div class="panel-body">
		<div class="row">

			<div class="form-group">
			<label class="col-sm-3 control-label">Jenjang*</label>

			<?php
				
				$sd="";$smp="";$sma="";$d3="";$d4="";$s1="";$s2="";$s3="";$d1="";$d2="";

				if($rwp->jenjang=="SD/sederajat")
				{
					$sd="selected";
				}
				else if($rwp->jenjang=="SMP/sederajat")
				{
					$smp="selected";
				}
				else if($rwp->jenjang=="SMA/sederajat")
				{
					$sma="selected";
				}

				else if($rwp->jenjang=="D1")
				{
					$d1="selected";
				}
				else if($rwp->jenjang=="D2")
				{
					$d2="selected";
				}
				else if($rwp->jenjang=="D3")
				{
					$d3="selected";
				}
				else if($rwp->jenjang=="D4")
				{
					$d4="selected";
				}
				else if($rwp->jenjang=="S1")
				{
					$s1="selected";
				}
				else if($rwp->jenjang=="S2")
				{
					$s2="selected";
				}
				else if($rwp->jenjang=="S3")
				{
					$s3="selected";
				}


			?>

				<div class="col-sm-3">
				<select ui-jq="choosen" class="form-control" name="jenjang">
					<optgroup label="Pilih Jenjang">
						<option value="SD/sederajat" {{ $sd }} >SD/sederajat</option>
						<option value="SMP/sederajat" {{ $smp }} >SMP/sederajat</option>
						<option value="SMA/sederajat" {{ $sma }} >SMA/sederajat</option>
						<option value="D1" {{ $d1 }}>D1</option>
						<option value="D2" {{ $d2 }}>D2</option>
						<option value="D3" {{ $d3 }} >D3</option>
						<option value="D4" {{ $d4 }} >D4</option>
						<option value="S1" {{ $s1 }} >S1</option>
						<option value="S2" {{ $s2 }}>S2</option>
						<option value="S3" {{ $s3 }}>S3</option>
						
					</optgroup>
				</select>
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Nama Sekolah*</label>

				<div class="col-sm-4">
					<input type="text" class="form-control text-uppercase" name="nama_sekolah" autocomplete="off" value="{{ $rwp->nama_sekolah }}">
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Bidang Studi</label>

				<div class="col-sm-4">
				<input type="text" name="bidang_studi" class="form-control text-uppercase" autocomplete="off" value="{{ $rwp->bidang_studi}}">
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Fakultas</label>

				<div class="col-sm-3">
					<input type="text" class="form-control text-uppercase" name="fakultas" autocomplete="off" value="{{ $rwp->fakultas }}">
				</div>

			</div>

			

			<div class="form-group">
			<label class="col-sm-3 control-label">Kependidikan</label>

			<?php
				$y="";
				$n="";

				if($rwp->kependidikan=="ya")
				{
					$y="checked";
				}
				else
				{
					$n="checked";
				}
			?>

				<div class="col-sm-3">
				<div class="radio">
					<label><input type=radio name="kependidikan" value="ya" {{$y}} >Ya</label>
					<label><input type=radio name="kependidikan" value="tidak" {{ $n}}> Tidak</label>
				</div>
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Tahun Masuk*</label>

				<div class="col-sm-2">
					<input type="text" name="tahun_masuk" class="form-control text-uppercase" autocomplete="off" value="{{ $rwp->tahun_masuk }}" maxlength="4" onkeypress="return hanyaAngka(event)"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Tahun Lulus*</label>

				<div class="col-sm-2">
					<input type="text" name="tahun_lulus" class="form-control text-uppercase" autocomplete="off" value="{{ $rwp->tahun_lulus }}" maxlength="4" onkeypress="return hanyaAngka(event)"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">NIM/NIS*</label>

				<div class="col-sm-2">
					<input type="text" name="NIM" class="form-control" autocomplete="off" value="{{ $rwp->NIM }}"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Status*</label>
			<?php
				$y="";
				$n="";
				if($rwp->status=="Lulus"){
					$n="checked";
				}else{
					$y="checked";
				}
			?>
				<div class="col-sm-3">
				<div class="radio">
					<label><input type="radio" name="status" value="Masih Bersekolah" {{ $y }}>Masih Bersekolah</label>
					<label><input type="radio" name="status" value="Lulus" {{ $n }}>Lulus</label>
				</div>
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Semester*</label>

				<div class="col-sm-1">
					<input type="text" name="semester" class="form-control" autocomplete="off" value="{{ $rwp->semester }}"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">IPK</label>

				<div class="col-sm-2">
					<input type="text" name="IPK" class="form-control" autocomplete="off" value="{{ $rwp->IPK }}"> 
				</div>

			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Gelar</label>

				<div class="col-sm-3">
					<input type="text" class="form-control" name="gelar" autocomplete="off" value="{{ $rwp->gelar }}">
				</div>

			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>

	<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>

	<br>
	<br> 

	{{ csrf_field() }}

	<input type="hidden" name="_method" value="PUT">

</form>

<script>

$(document).ready(function(){
	$('.expander-drop').slideDown(200);
	$('.expander').click(function(){

		$(this).parent().next().slideToggle(200);
	});
	//$('.expander-drop').slideUp(200);


});
</script>

@endsection
