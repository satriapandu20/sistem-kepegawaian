@extends ('layout/master')
@section ('title','Pensiun')
@section ('content')

<div class="row">

    <div class="col-lg-12">
    <div class="page-header">

        <h3>Daftar Pensiun - Tahun {{$tahun}}</h3>
        
     </div>
     </div>
</div>

<div class="col-xs-4 pull-right">
        	<input type="text" class="form-control" placeholder="Cari...">
            
       	</div>


<br>
<br>
<br>   
	

	<table class="table table-striped table-bordered table-hover">
	<thead>

	<tr>
		<th>#</th>
		<th>NIK</th>
		<th class="col-lg-5">Nama</th>
		<th align="center">Kirim Notifikasi</th>

	</tr>
	</thead>

	<tbody>

	@foreach($pg as $pg)
	<tr>
		<td></td>
		<td class="text-uppercase">{{$pg->NIK}}</td>
		<td class="text-uppercase">{{$pg->nama}}</td>
		<td align="center">
		<a href="{{ url('pegawai/'.$pg->id_pegawai.'/pensiun_mail') }}"><button type="button" class="btn btn-success">Email</button></a>
                      <a href="{{ url('pegawai/'.$pg->id_pegawai.'/pensiun_sms') }}"><button type="button" name="button" class="btn btn-warning">SMS</button></a>

		</td>

	</tr>

	@endforeach

	</tbody>

	</table>
	</div>
</div>
</div>

@endsection