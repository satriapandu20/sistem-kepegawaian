@extends ('layout/print-master')
@section ('title','Pegawai')
@section ('content')

<div class="row">

    <div class="col-lg-12">
    <div class="page-header">

        <h3>Halaman All</h3>
        
     </div>
     </div>
</div>

	<form class="form-horizontal row">

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Nama :</label>
				<p class="form-control-static text-uppercase">{{$pg->nama}}</p>

		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">NIK :</label>
				<p class="form-control-static text-uppercase">{{$pg->NIK}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Jenis Kelamin :</label>
				<p class="form-control-static text-uppercase">{{$pg->jenis_kelamin}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Tempat Lahir :</label>
				<p class="form-control-static text-uppercase">{{$pg->tempat_lahir}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Tanggal Lahir</label>
				<p class="form-control-static text-uppercase">{{$pg->tanggal_lahir}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Agama :</label>
				<p class="form-control-static text-uppercase">{{$pg->agama}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Status :</label>
				<p class="form-control-static text-uppercase">{{$pg->status_perkawinan}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Nama Ibu:</label>
				<p class="form-control-static text-uppercase">{{$pg->nama_ibu_kandung}}</p>
				
		</div>
		
	</div>

	<br>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Alamat Lengkap :</label>
				<p class="form-control-static text-uppercase">{{$pg->alamat}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">RT :</label>
				<p class="form-control-static text-uppercase">{{$pg->RT}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">RW :</label>
				<p class="form-control-static text-uppercase">{{$pg->RW}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kelurahan :</label>
				<p class="form-control-static text-uppercase">{{$pg->nama}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kecamatan :</label>
				<p class="form-control-static text-uppercase">{{$pg->kecamatan}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kabupaten</label>
				<p class="form-control-static text-uppercase">{{$pg->kabupaten}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kode Pos</label>
				<p class="form-control-static text-uppercase">{{$pg->kode_pos}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Nomor Telepon :</label>
				<p class="form-control-static text-uppercase">{{$pg->nomor_telepon}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">NPWP :</label>
				<p class="form-control-static text-uppercase">{{$pg->NPWP}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Nama Wajib Pajak:</label>
				<p class="form-control-static text-uppercase">{{$pg->nama_wajib_pajak}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Nama :</label>
				<p class="form-control-static text-uppercase">{{$pg->nama}}</p>
				
		</div>
		
	</div>

	<br>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kartu Pegawai :</label>
				<p class="form-control-static text-uppercase">{{$pg->kartu_pegawai}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Jenis GTK :</label>
				<p class="form-control-static text-uppercase">{{$pg->jenis_gtk}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">NUPTK :</label>
				<p class="form-control-static text-uppercase">{{$pg->NUPTK}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Status Kepegawaian:</label>
				<p class="form-control-static text-uppercase">{{$pg->status_kepegawaian}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">NIP :</label>
				<p class="form-control-static text-uppercase">{{$pg->NIP}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">SK CPNS :</label>
				<p class="form-control-static text-uppercase">{{$pg->SK_CPNS}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">TMT CPNS :</label>
				<p class="form-control-static text-uppercase">{{$pg->TMT_CPNS}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">TMT SK Pengangkatan:</label>
				<p class="form-control-static text-uppercase">{{$pg->TMT_SK_pengangkatan}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Sumber Gaji :</label>
				<p class="form-control-static text-uppercase">{{$pg->sumber_gaji}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Masa Kerja :</label>
				<p class="form-control-static text-uppercase">{{$pg->masa_kerja}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Tahun Pensiun :</label>
				<p class="form-control-static text-uppercase">{{$pg->tahun_pensiun}}</p>
				
		</div>
		
	</div>

	<br>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Nama Pasangan:</label>
				<p class="form-control-static text-uppercase">{{$pg->nama_pasangan}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Pekerjaan Pasangan :</label>
				<p class="form-control-static text-uppercase">{{$pg->pekerjaan_pasangan}}</p>
				
		</div>
		
	</div>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kartu Pasangan :</label>
				<p class="form-control-static text-uppercase">{{$pg->kartu_pasangan}}</p>
				
		</div>
		
	</div>

	<br>

	<div class="row">

		<div class="form-group">

				<label class="col-sm-3 control-label">Kartu Pasangan :</label>
				<p class="form-control-static text-uppercase">{{$pg->kartu_pasangan}}</p>
				
		</div>
		
	</div>

	</form>


@endsection