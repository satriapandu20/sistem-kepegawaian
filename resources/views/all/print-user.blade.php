
<html>

<head><title>Cetak</title></head>
<style type="text/css">
body{
    width: 100%;
    margin:auto;
    padding-top:-10;

}
.header{
    text-align: center;
    letter-spacing: 0.5px;
    line-height: 1.5;
}
.atas{
    font-size:18px;
}
.bawah{
    font-size: 14px;
}
table{
    width: 100%;

}
.isian table, .isian th, .isian td{
    border:1px solid #000;
    border-collapse: collapse;
    padding: 5px;
}

.content{
    


}

td p{
    text-transform: uppercase;
    padding-top: -13px;
    padding-bottom: -13px;
}
</style>
<body>

<div class="content">

    <table>
    <tr>
    <td><img src="http://localhost/payschool/public/images/ttwhyn.png" width="150"></td>
    <td>
        <div class="header">
                 <p>
                <div class="atas">
                PEMERINTAH PROVINSI JAWA TENGAH <br> DINAS PENDIDIKAN
                </div>
                <b style="font-size: 24px;text-decoration:underline;">SMKN 1 PURWOKERTO</b>
                <br>
                <div class="bawah">
                NPSN: 20330459, JL. DR. SOEPARNO NO. 29 RT/RW: 003/005 Kec. Purwokerto Timur Kab. Banyumas Prov.
                Jawa Tengah Telp.0281/637132 Fax.0281/637132 Email: admin@smkn1purwokerto.sch.id
                </div>
                </p>
        </div>
    </td>
    </tr>
    </table>
    <hr style="height: 1px; margin-top:0; background: #000;">



    <table>

    <tr><td>
    <div class="header">
    <b style="font-size: 18px;text-align: center;text-decoration: underline;">Profil Guru</b>

    </div>
    <br>
    </td></tr>

    </table>
    <div class="isian">
    <table>
        <tr>
            <th>Atribut</th>
            <th>Isian</th>
        </tr>
        <tr><td class="label"><label >Nama</label></td>
                
                <td><p class="text-uppercase">{{$pg->nama}}</td></tr>


                <tr><td class="label"><label >NIK</label></td>
                
                <td><p >{{$pg->NIK}}</td></tr>
                
    


                <tr><td class="label"><label >Jenis Kelamin</label></td>
                
                <td><p >{{$pg->jenis_kelamin}}</td></tr>
                




                <tr><td class="label"><label >Tempat Lahir</label></td>
                
                <td><p >{{$pg->tempat_lahir}}</td></tr>
                


                <tr><td class="label"><label >Tanggal Lahir</label></td>
                
                <td><p >{{$pg->tanggal_lahir}}</td></tr>
                


                <tr><td class="label"><label >Agama</label></td>
                
                <td><p >{{ $pg->agama }}</td></tr>
                

                <tr><td class="label"><label >Status</label></td>
                
                <td><p >{{$pg->status_perkawinan}}</td></tr>
                

                <tr><td class="label"><label >Nama Ibu</label></td>
                
                <td><p >{{$pg->nama_ibu_kandung}}</td></tr>
                

                <tr><td class="label"><label >Alamat Lengkap</label></td>
                
                <td><p >{{$pg->alamat}}</td></tr>
                

                <tr><td class="label"><label >RT</label></td>
                
                <td><p >{{$pg->RT}}</td></tr>
                

                <tr><td class="label"><label >RW</label></td>
                
                <td><p >{{$pg->RW}}</td></tr>
                

                <tr><td class="label"><label >Kelurahan</label></td>
                
                <td><p >{{$pg->nama}}</td></tr>
                

                <tr><td class="label"><label >Kecamatan</label></td>
                
                <td><p >{{$pg->kecamatan}}</td></tr>
                

                <tr><td class="label"><label >Kabupaten</label></td>
                
                <td><p >{{$pg->kabupaten}}</td></tr>
                

                <tr><td class="label"><label >Kode Pos</label></td>
                
                <td><p >{{$pg->kode_pos}}</td></tr>

                <tr><td class="label"><label >Nomor Telepon</label></td>
                
                <td><p >{{$pg->nomor_telepon}}</td></tr>

                <tr><td class="label"><label >NPWP</label></td>
                
                <td><p >{{$pg->NPWP}}</td></tr>
                

                <tr><td class="label"><label >Nama Wajib Pajak</label></td>
                
                <td><p >{{$pg->nama_wajib_pajak}}</td></tr>
                

                <tr><td class="label"><label >Nama</label></td>
                
                <td><p >{{$pg->nama}}</td></tr>
                

                <tr><td class="label"><label >Kartu Pegawai</label></td>
                
                <td><p >{{$pg->kartu_pegawai}}</td></tr>
                

                <tr><td class="label"><label >Jenis GTK</label></td>
                
                <td><p >{{$pg->jenis_gtk}}</td></tr>
                

                <tr><td class="label"><label >NUPTK</label></td>
                
                <td><p >{{$pg->NUPTK}}</td></tr>
                

                <tr><td class="label"><label >Status Kepegawaian</label></td>
                
                <td><p >{{$pg->status_kepegawaian}}</td></tr>
                
                <tr><td class="label"><label >NIP</label></td>
                
                <td><p >{{$pg->NIP}}</td></tr>
                

                <tr><td class="label"><label >SK CPNS</label></td>
                
                <td><p >{{$pg->SK_CPNS}}</td></tr>
                

                <tr><td class="label"><label >TMT CPNS</label></td>
                
                <td><p >{{$pg->TMT_CPNS}}</td></tr>
                

                <tr><td class="label"><label >TMT SK Pengangkatan</label></td>
                
                <td><p >{{$pg->TMT_SK_pengangkatan}}</td></tr>
                

                <tr><td class="label"><label >Sumber Gaji</label></td>
                
                <td><p >{{$pg->sumber_gaji}}</td></tr>
                

                <tr><td class="label"><label >Masa Kerja</label></td>
                
                <td><p >{{$pg->masa_kerja}}</td></tr>
                

                <tr><td class="label"><label >Tahun Pensiun</label></td>
                
                <td><p >{{$pg->tahun_pensiun}}</td></tr>
                
                <tr><td class="label"><label >Nama Pasangan</label></td>
                
                <td><p >{{$pg->nama_pasangan}}</td></tr>

                <tr><td class="label"><label >Pekerjaan Pasangan</label></td>
                
                <td><p >{{$pg->pekerjaan_pasangan}}</td></tr>


                <tr><td class="label"><label >Kartu Pasangan</label></td>
                
                <td><p >{{$pg->kartu_pasangan}}</td></tr>

    </table>
    </div>
    <br>

    <table>
        <tr>
        <td width="60%"></td>
        <td>
        <?php

        date_default_timezone_set('Asia/Jakarta');
            $y=date('Y');
            $d=date('d');
            $m=date('m');

            if($m=='01'){
                $m='Januari';
            }else if($m=='02'){
                $m='Februari';
            }else if($m=='03'){
                $m='Maret';
            }else if($m=='04'){
                $m='April';
            }else if($m=='05'){
                $m='Mei';
            }else if($m=='06'){
                $m='Juni';
            }else if($m=='07'){
                $m='Juli';
            }else if($m=='08'){
                $m='Agustus';
            }else if($m=='09'){
                $m='September';
            }else if($m=='10'){
                $m='Oktober';
            }else if($m=='11'){
                $m='November';
            }else if($m=='12'){
                $m='Desember';
            }

        ?>
            <p style="text-transform: none;">Kab. Banyumas, {{$d}} {{$m}} {{$y}} <br> Menyetujui,</p>
            <br>
            <br>
            <br>
            <p>
            <b style="text-decoration: underline;text-transform: uppercase;">{{$pg->nama}}</b>
            <br>
            <?php
            if($pg->NIP!=""||$pg->NIP!=null){
            ?>
            NIP. {{$pg->NIP}}
            <?php 
            }else{
            ?>
            NIK. {{$pg->NIK}}
            <?php } ?>
            </p>
        </td>
        </tr>
    </table>

</div>

</body>
</html>