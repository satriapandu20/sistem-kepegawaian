@extends ('layout/print-master')
@section ('title','Pegawai')
@section ('content')

<div id="judul">

        <h2>SMK NEGERI 1 PURWOKERTO</h2>
        <p>Data Kepegawaian</p>
      
</div>

<hr>

<br> 
<table>
	<tr>
		<th>Atribut</th>
		<th>Isian</th>
	</tr>
				<tr><td class="label"><label >Nama</label></td>
				
				<td><p class="text-uppercase">{{$pg->nama}}</td></tr>


				<tr><td class="label"><label >NIK</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->NIK}}</td></tr>
				
	


				<tr><td class="label"><label >Jenis Kelamin</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->jenis_kelamin}}</td></tr>
				




				<tr><td class="label"><label >Tempat Lahir</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->tempat_lahir}}</td></tr>
				


				<tr><td class="label"><label >Tanggal Lahir</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->tanggal_lahir}}</td></tr>
				


				<tr><td class="label"><label >Agama</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->agama}}</td></tr>
				

				<tr><td class="label"><label >Status</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->status_perkawinan}}</td></tr>
				

				<tr><td class="label"><label >Nama Ibu</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->nama_ibu_kandung}}</td></tr>
				

				<tr><td class="label"><label >Alamat Lengkap</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->alamat}}</td></tr>
				

				<tr><td class="label"><label >RT</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->RT}}</td></tr>
				

				<tr><td class="label"><label >RW</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->RW}}</td></tr>
				

				<tr><td class="label"><label >Kelurahan</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->nama}}</td></tr>
				

				<tr><td class="label"><label >Kecamatan</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->kecamatan}}</td></tr>
				

				<tr><td class="label"><label >Kabupaten</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->kabupaten}}</td></tr>
				

				<tr><td class="label"><label >Kode Pos</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->kode_pos}}</td></tr>

				<tr><td class="label"><label >Nomor Telepon</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->nomor_telepon}}</td></tr>

				<tr><td class="label"><label >NPWP</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->NPWP}}</td></tr>
				

				<tr><td class="label"><label >Nama Wajib Pajak</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->nama_wajib_pajak}}</td></tr>
				

				<tr><td class="label"><label >Nama</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->nama}}</td></tr>
				

				<tr><td class="label"><label >Kartu Pegawai</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->kartu_pegawai}}</td></tr>
				

				<tr><td class="label"><label >Jenis GTK</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->jenis_gtk}}</td></tr>
				

				<tr><td class="label"><label >NUPTK</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->NUPTK}}</td></tr>
				

				<tr><td class="label"><label >Status Kepegawaian</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->status_kepegawaian}}</td></tr>
				
				<tr><td class="label"><label >NIP</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->NIP}}</td></tr>
				

				<tr><td class="label"><label >SK CPNS</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->SK_CPNS}}</td></tr>
				

				<tr><td class="label"><label >TMT CPNS</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->TMT_CPNS}}</td></tr>
				

				<tr><td class="label"><label >TMT SK Pengangkatan</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->TMT_SK_pengangkatan}}</td></tr>
				

				<tr><td class="label"><label >Sumber Gaji</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->sumber_gaji}}</td></tr>
				

				<tr><td class="label"><label >Masa Kerja</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->masa_kerja}}</td></tr>
				

				<tr><td class="label"><label >Tahun Pensiun</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->tahun_pensiun}}</td></tr>
				
				<tr><td class="label"><label >Nama Pasangan:</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->nama_pasangan}}</td></tr>

				<tr><td class="label"><label >Pekerjaan Pasangan</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->pekerjaan_pasangan}}</td></tr>


				<tr><td class="label"><label >Kartu Pasangan</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->kartu_pasangan}}</td></tr>

				<tr><td class="label"><label >Kartu Pasangan</label></td>
				
				<td><p class="form-control-static text-uppercase">{{$pg->kartu_pasangan}}</td></tr>
				
		</div>
		
	</div>

	</form>

</table>

@endsection