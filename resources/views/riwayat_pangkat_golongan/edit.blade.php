@extends('layout/master')
@section('title','Riwayat Pangkat Golongan')
@section('content')

<div class="row">

    <div class="col-lg-12">

        <h4 class="page-header text-uppercase">Riwayat Pangkat Golongan {{ $pg->nama }} </h4>
     </div>
</div>


	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan/'.$rpg->id_riwayat_pangkat_golongan) }}" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Pangkat Golongan

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Pangkat Golongan</label>
	          <div class="col-lg-2">

	          <?php
	          	$IIIa="";
	          	$IIIb="";
	          	$IIIc="";
	          	$IIId="";
	          	$IVa="";
	          	$IVb="";
	          	$IVc="";
	          	$IVd="";
	          	$IVe="";

	          	if($rpg->pangkat_golongan=="III/a"){
	          		$IIIa="selected";
	          	}else if($rpg->pangkat_golongan=="III/b"){
	          		$IIIb="selected";
	          	}else if($rpg->pangkat_golongan=="III/c"){
	          		$IIIc="selected";
	          	}else if($rpg->pangkat_golongan=="III/d"){
	          		$IIId="selected";
	          	}else if($rpg->pangkat_golongan=="IV/a"){
	          		$IVa="selected";
	          	}else if($rpg->pangkat_golongan=="IV/b"){
	          		$IVb="selected";
	          	}else if($rpg->pangkat_golongan=="IV/c"){
	          		$IVc="selected";
	          	}else if($rpg->pangkat_golongan=="IV/d"){
	          		$IVd="selected";
	          	}else if($rpg->pangkat_golongan=="IV/e"){
	          		$IVe="selected";
	          	}

	          ?>
	            <select ui-jq="chosen" class="form-control" name="pangkat_golongan">
	                <optgroup label="Pangkat Golongan">
	                    <option value="III/a" {{ $IIIa }} >III/a</option>
	                    <option value="III/b" {{ $IIIb }} >III/b</option>
	                    <option value="III/c" {{ $IIIc }} >III/c</option>
	                    <option value="III/d" {{ $IIId }} >III/d</option>
	                    <option value="IV/a" {{ $IVa }} >IV/a</option>
	                    <option value="IV/b" {{ $IVb }} >IV/b</option>
	                    <option value="IV/c" {{ $IVc }} >IV/c</option>
	                    <option value="IV/d" {{ $IVd }} >IV/d</option>
	                    <option value="IV/e" {{ $IVe }} >IV/e</option>

	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Nomor SK</label>
				<div class="col-sm-3">
				<input type="text" name="no_sk" class="form-control text-uppercase" autocomplete="off" value="{{ $rpg->no_sk}}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal SK</label>
				<div class="col-sm-1">
				<input type="text" name="tanggal_sk" class="form-control text-uppercase" autocomplete="off" value="{{ $tgl_sk['d'] }}">
				</div>

				<?php 
				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tgl_sk['m']=="01")
				{
					$Januari="selected";
				}
				else if($tgl_sk['m']=="02")
				{
					$Februari="selected";
				}
				else if($tgl_sk['m']=="03")
				{
					$Maret="selected";
				}
				else if($tgl_sk['m']=="04")
				{
					$April="selected";
				}
				else if($tgl_sk['m']=="05")
				{
					$Mei="selected";
				}
				else if($tgl_sk['m']=="06")
				{
					$Juni="selected";
				}
				else if($tgl_sk['m']=="07")
				{
					$Juli="selected";
				}
				else if($tgl_sk['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tgl_sk['m']=="09")
				{
					$September="selected";
				}
				else if($tgl_sk['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tgl_sk['m']=="11")
				{
					$November="selected";
				}
				else if($tgl_sk['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_sk">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_sk" autocomplete="off" value="{{ $tgl_sk['y']}}">
				</div>

			</div>

			     <div class="form-group">
				<label class="col-sm-3 control-label">TMT Pangkat Gologan</label>
				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt" class="form-control text-uppercase" autocomplete="off" value="{{ $tmt_pg['d'] }}">
				</div>

				<?php 
				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tmt_pg['m']=="01")
				{
					$Januari="selected";
				}
				else if($tmt_pg['m']=="02")
				{
					$Februari="selected";
				}
				else if($tmt_pg['m']=="03")
				{
					$Maret="selected";
				}
				else if($tmt_pg['m']=="04")
				{
					$April="selected";
				}
				else if($tmt_pg['m']=="05")
				{
					$Mei="selected";
				}
				else if($tmt_pg['m']=="06")
				{
					$Juni="selected";
				}
				else if($tmt_pg['m']=="07")
				{
					$Juli="selected";
				}
				else if($tmt_pg['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tmt_pg['m']=="09")
				{
					$September="selected";
				}
				else if($tmt_pg['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tmt_pg['m']=="11")
				{
					$November="selected";
				}
				else if($tmt_pg['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" autocomplete="off" name="tahun_tmt" value="{{ $tmt_pg['y']}}">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja Tahun</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja_tahun" class="form-control text-uppercase" autocomplete="off" value="{{ $rpg->masa_kerja_tahun}}">
				<span class="input-group-addon">Tahun</span>
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja Bulan</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja_bulan" class="form-control text-uppercase" autocomplete="off" value="{{$rpg->masa_kerja_bulan}}" >
				<span class="input-group-addon">Bulan</span>
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Lembaga Pengangkat</label>
				<div class="col-sm-3">
				<input type="text" name="lembaga_pengangkat" class="form-control text-uppercase" autocomplete="off" value="{{$rpg->lembaga_pengangkat}}">
				</div>
			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>



		<?php 
            if(Session::get('admin'))
            {
        ?>
		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>
		<?php
			}

		?>
		
		<br>
		<br>


		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">
	
	</form>


<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });
  

</script>  

<script>
	
	$(document).ready(function () {
   	//$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    //$('.expander-drop').slideUp(200);
	});	

</script>


@endsection
