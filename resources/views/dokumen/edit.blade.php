@extends('layout/master')
@section('title','Dokumen')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div  class="page-header">

        <h4 class="text-uppercase">Halaman Edit Dokumen</h4>
        
		<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>

    </div>
    </div>
</div>


	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/dokumen/'.$doc->id_dokumen) }}" method="post" enctype="multipart/form-data">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Dokumen

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">

        	
			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Dokumen</label>
				<div class="col-sm-4">
				<input type="text" name="nama_dokumen" autocomplete="off" class="form-control text-uppercase" value="{{ $doc->nama_dokumen }}">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Masukan Dokumen</label>
				<div class="col-sm-4">
				<input type="file" name="dokumen" autocomplete="off" class=""  accept="application/pdf, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
				</div>
				<p style="color:red;">*Masukan dokumen baru untuk mengubah dokumen</p>
			</div>

			<?php 
				$gaji_berkala="";
				$lainnya="";

				if($doc->jenis=="gaji_berkala")
				{
					$gaji_berkala="checked";

				}else if($doc->jenis=="lainnya"){

					$lainnya="checked";
				}
			?>
			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Dokumen</label>
				<div class="col-sm-4">
				<input type="radio" name="jenis" {{ $gaji_berkala }} value="gaji_berkala"> Gaji Berkala
				<input type="radio" name="jenis" {{ $lainnya }} value="lainnya"> Lainnya
				</div>
			</div>

		

			<div class="form-group">
				<label class="col-sm-3 control-label">Keterangan</label>
				<div class="col-sm-8">

				<textarea  name="keterangan" class="form-control text-uppercase" rows="4" autocomplete="off">{{$doc->keterangan}}</textarea>
				

				</div>
			</div>

			

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">
	</form>





<script>
	
	$(document).ready(function () {
   	$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    $('.expander-drop').slideUp(200);
	});	

</script>


@endsection
