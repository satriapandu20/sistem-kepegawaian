@extends('layout/master')
@section('title','Riwayat Gaji Berkala')
@section('content')

<div class="row">

    <div class="col-lg-12">

        <h4 class="page-header text-uppercase">Riwayat Gaji Berkala {{ $pg->nama }}</h4>
     </div>
</div>

	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala/'.$rgb->id_riwayat_gaji_berkala) }}" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Riwayat Gaji Berkala

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Pangkat Golongan</label>
	          <div class="col-lg-2">
	            <select ui-jq="chosen" class="form-control" name="pangkat_golongan">
	                <optgroup label="Pangkat Golongan">

	                @foreach($rpgs as $rpg)

	                @if($rpg->id_riwayat_pangkat_golongan==$rgb->id_pangkat_golongan)
	                <option value="{{ $rpg->id_riwayat_pangkat_golongan }}" selected="selected">{{$rpg->pangkat_golongan}}</option>
	                @else
	                <option value="{{ $rpg->id_riwayat_pangkat_golongan }}">{{$rpg->pangkat_golongan}}</option>
	                @endif


	                @endforeach
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Nomor SK</label>
				<div class="col-sm-3">
				<input type="text" name="no_sk" class="form-control text-uppercase" autocomplete="off" value="{{$rgb->no_sk}}" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal SK</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_sk" class="form-control text-uppercase" autocomplete="off" value="{{ $tgl_sk['d'] }}">
				</div>

				<?php 
				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tgl_sk['m']=="01")
				{
					$Januari="selected";
				}
				else if($tgl_sk['m']=="02")
				{
					$Februari="selected";
				}
				else if($tgl_sk['m']=="03")
				{
					$Maret="selected";
				}
				else if($tgl_sk['m']=="04")
				{
					$April="selected";
				}
				else if($tgl_sk['m']=="05")
				{
					$Mei="selected";
				}
				else if($tgl_sk['m']=="06")
				{
					$Juni="selected";
				}
				else if($tgl_sk['m']=="07")
				{
					$Juli="selected";
				}
				else if($tgl_sk['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tgl_sk['m']=="09")
				{
					$September="selected";
				}
				else if($tgl_sk['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tgl_sk['m']=="11")
				{
					$November="selected";
				}
				else if($tgl_sk['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_sk">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_sk" autocomplete="off" value="{{ $tgl_sk['y']}}">
				</div>

			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT KGB</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt" class="form-control" autocomplete="off" value="{{ $tmt_gb['d'] }}">
				</div>

				
				<?php 
				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tmt_gb['m']=="01")
				{
					$Januari="selected";
				}
				else if($tmt_gb['m']=="02")
				{
					$Februari="selected";
				}
				else if($tmt_gb['m']=="03")
				{
					$Maret="selected";
				}
				else if($tmt_gb['m']=="04")
				{
					$April="selected";
				}
				else if($tmt_gb['m']=="05")
				{
					$Mei="selected";
				}
				else if($tmt_gb['m']=="06")
				{
					$Juni="selected";
				}
				else if($tmt_gb['m']=="07")
				{
					$Juli="selected";
				}
				else if($tmt_gb['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tmt_gb['m']=="09")
				{
					$September="selected";
				}
				else if($tmt_gb['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tmt_gb['m']=="11")
				{
					$November="selected";
				}
				else if($tmt_gb['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" autocomplete="off" name="tahun_tmt" value="{{ $tmt_gb['y']}}">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja Tahun</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja_tahun" class="form-control text-uppercase" autocomplete="off" value="{{ $rgb->masa_kerja_tahun }}">
				<span class="input-group-addon">Tahun</span>
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja Bulan</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja_bulan" class="form-control text-uppercase" autocomplete="off" value="{{ $rgb->masa_kerja_bulan }}">
				<span class="input-group-addon">Bulan</span>
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Gaji Pokok</label>
				<div class="col-lg-3">
					<div class="input-group">
					<span class="input-group-addon">Rp</span>
					<input type="text" class="form-control" autocomplete="off" name="gaji_pokok" value="{{ $rgb->gaji_pokok }}">
					</div>
				</div>

			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>



		<?php 
            if(Session::get('admin'))
            {
        ?>
		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>
		<?php } ?>
		<br>
		<br>


		{{ csrf_field() }}
	<input type="hidden" name="_method" value="PUT">
	</form>

<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });
  

</script>  

<script>
	
	$(document).ready(function () {
   	//$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    //$('.expander-drop').slideUp(200);
	});	

</script>


@endsection
