@extends('layout/master')
@section('title','Tambah Riwayat Gaji Berkala')
@section('content')

<div class="row">

    <div class="col-lg-12">

        <h4 class="page-header">Tambah Riwayat Gaji Berkala</h4>
     </div>
</div>

	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Riwayat Gaji Berkala

        </div>
        </div>
		
		<div class="expander-drop">
        <div class="panel-body">
        <div class="row">

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Pangkat Golongan</label>
	          <div class="col-lg-2">
	            <select ui-jq="chosen" class="form-control" name="pangkat_golongan">
	                <optgroup label="Pangkat Golongan">

	                @foreach($rpgs as $rpg)
	                <option value="{{ $rpg->id_riwayat_pangkat_golongan }}">{{$rpg->pangkat_golongan}}</option>

	                @endforeach
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Nomor SK</label>
				<div class="col-sm-3">
				<input type="text" name="no_sk" class="form-control text-uppercase" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal SK</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_sk" class="form-control text-uppercase" autocomplete="off">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_sk">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_sk" autocomplete="off" placeholder="Tahun">
				</div>

			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">TMT KGB</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_tmt" class="form-control" autocomplete="off">
				</div>

				
				<div class="col-sm-2">
				<select class="form-control" name="bulan_tmt">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" autocomplete="off" name="tahun_tmt" placeholder="Tahun">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja Tahun</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja_tahun" class="form-control text-uppercase" autocomplete="off">
				<span class="input-group-addon">Tahun</span>
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Masa Kerja Bulan</label>
				<div class="col-lg-2">

				<div class="input-group">
				<input type="text" name="masa_kerja_bulan" class="form-control text-uppercase" autocomplete="off">
				<span class="input-group-addon">Bulan</span>
				</div>

				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Gaji Pokok</label>
				<div class="col-lg-3">
					<div class="input-group">
					<span class="input-group-addon">Rp</span>
					<input type="text" class="form-control" autocomplete="off" name="gaji_pokok">
					</div>
				</div>

			</div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>



		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Tambah</button>
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
	
	</form>


<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });
  

</script>  

<script>
	
	$(document).ready(function () {
   	//$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    //$('.expander-drop').slideUp(200);
	});	

</script>


@endsection
