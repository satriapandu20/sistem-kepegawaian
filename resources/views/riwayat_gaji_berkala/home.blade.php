@extends ('layout/master')
@section ('title','Riwayat Gaji Berkala')
@section ('content')
<!-- 
RIWAYAT HOME
 -->
<div class="row">

    <div class="col-lg-12">
    <div class="page-header">
        <h4 class="text-uppercase">Riwayat Gaji Berkala - {{ $pg->nama }}</h4>

	<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
    </div>

    @if($rpg_count!=0)
	<button class="btn btn-primary"><a style="color:#fff;	text-decoration: none;"href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala/create')}}">Tambah Riwayat Gaji Berkala</a></button>
    @else
    <p style="color:red;">*Pegawai belum dapat memperoleh gaji berkala ! <br>&nbsp;Catatan: Dapat memperoleh gaji berkala jika telah memiliki pangkat golongan.</p>
    @endif

    </div>
    <?php } ?>


</div>
<br>


	<!-- <a href="{{ url('pegawai/create') }}">Tambah</a> -->

	<table class="table table-striped table-bordered table-hover">
	<thead>

	<tr>
		<th>#</th>
		<th class="col-lg-3">Pangkat Golongan</th>
		<th class="col-lg-3">Nomor SK</th>
		<th class="col-lg-3">TMT Gaji Berkala</th>
		<th>Aksi</th>

	</tr>
	</thead>

	<tbody>

	@foreach($rgbs as $rgb)
	<tr>
		<td></td>
		<td class="text-uppercase">{{ $rgb->pangkat_golongan}}</td>
		<td class="text-uppercase">{{ $rgb->no_sk}}</td>
		<td class="text-uppercase">{{ $rgb->TMT_KGB }}</td>
		<td align="center">
		<a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala/'.$rgb->id_riwayat_gaji_berkala) }}"><button type="button" class="btn btn-info"><i class="fa fa-edit"></i></button></a>

		<?php 
            if(Session::get('admin'))
            {
        ?>
                       <a href="#"><button type="button" name="button" class="btn btn-danger deleteUser" data-userid="{{$rgb->id_riwayat_gaji_berkala}}" data-pegawaiid="{{$rgb->id_pegawai}}"><i class="fa fa-trash"></i></button></a>
        <?php 
    		}
        ?>

		</td>

	</tr>

	@endforeach

	</tbody>

	</table>
	</div>
</div>
</div>

<div id="applicantDeleteModal" class="modal modal-danger fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
             <form action="{{route('gaji_delete')}}" method="POST" class="remove-record-model">
               {{ method_field('delete') }}
               {{ csrf_field() }}

            <div class="modal-header"  style="background: #B22222;color: #fff;">
                
                <h4 class="modal-title text-center" id="custom-width-modalLabel">Hapus data gaji berkala ?</h4>
            </div>
            <div class="modal-body">
                <h5>Tekan <b>Hapus</b> untuk menghapus data gaji berkala.</h5>
                <input type="hidden" name="id_riwayat_gaji_berkala" id="app_id" value="">
                <input type="hidden" name="id_pegawai" id="app_id_p" value="">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger waves-effect remove-data-from-delete-form">Hapus</button>
            </div>

             </form>
        </div>
    </div>
</div>


<script type="text/javascript">

// function passID($id){
//     var delete_url="http://localhost/smecone/pegawai/"+id+"/non_aktif";
//     document.getElementById('delete_link').setAttribute('href' , 'FUCK');
//     $(".modal-footer #delete_link").setAttribute('href' , 'FUCK')
// }



</script>

<script>
$(document).on('click','.deleteUser',function(){
    var userID=$(this).attr('data-userid');
    var pegawaiID=$(this).attr('data-pegawaiid');

    $('#app_id').val(userID);
    $('#app_id_p').val(pegawaiID);

    $('#applicantDeleteModal').modal('show'); 
});
</script>
@endsection