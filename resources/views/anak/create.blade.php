@extends('layout/master')
@section('title','Halaman Anak')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div  class="page-header">

        <h4 class="text-uppercase">Halaman Tambah Anak</h4>
        
		<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>

    </div>
    </div>
</div>


	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Identitas Anak

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">

        	
			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Anak</label>
				<div class="col-sm-4">
				<input type="text" name="nama_anak" autocomplete="off" class="form-control text-uppercase" >
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Kelamin</label>
				<div class="col-sm-4">
				<div class="radio">
				<label><input type="radio" name="jenis_kelamin" value="L" >Laki-laki</label>
			  <label><input type="radio" name="jenis_kelamin" value="P" >Perempuan</label>
				</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tempat Lahir</label>
				<div class="col-sm-3">
				<input type="text" name="tempat_lahir" class="form-control text-uppercase" autocomplete="off" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Lahir</label>

				<div class="col-sm-1">
				<input type="text" name="tanggal_lahir" class="form-control text-uppercase" autocomplete="off" maxlength="2" onkeypress="return hanyaAngka(event)">
				</div>

				<div class="col-sm-2">
				<select class="form-control" name="bulan_lahir">
					<optgroup label="Bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_lahir" autocomplete="off" placeholder="Tahun" maxlength="4" onkeypress="return hanyaAngka(event)">
				</div>

			</div>
			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Status</label>
	          <div class="col-lg-3">
	            <select ui-jq="chosen" class="form-control" name="status">
	                <optgroup label="Pilih Status">
	                    <option value="Anak kandung">Anak Kandung</option>
	                    <option value="Anak asuh">Anak Asuh</option>
	                    <option value="Anak angkat">Anak Angkat</option>
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

			<div class="form-group">
				<label class="col-sm-3 control-label">NISN</label>
				<div class="col-sm-4">
				<input type="text" name="NISN" autocomplete="off" class="form-control text-uppercase">
				</div>
			</div>

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Jenjang</label>
	          <div class="col-lg-3">
	            <select ui-jq="chosen" class="form-control" name="jenjang">
	                <optgroup label="Pilih Jenjang">
	                    <option value="SD/sederajat">SD/sederajat</option>
	                    <option value="SMP/sederajat">SMP/sederajat</option>
	                    <option value="SMA/sederajat">SMA/sederajat</option>
	                    
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


	<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Tambah</button>
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
	
	</form>


<script type="text/javascript">

    $('.date').datepicker({  

       
       format : 'yyyy-mm-dd'



     });

</script>  


<script>
	
	$(document).ready(function () {
   	$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    $('.expander-drop').slideUp(200);
	});	

</script>


@endsection
