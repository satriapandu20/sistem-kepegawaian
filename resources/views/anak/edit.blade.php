@extends('layout/master')
@section('title','Halaman Anak')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div  class="page-header">

        <h4 class="text-uppercase">Halaman {{$anak->nama_anak}}</h4>
		
		<?php 
            if(Session::get('admin'))
            {
        ?>
            <a href="{{ url('pegawai/'.$pg->id_pegawai)}}"><button class="btn btn-outline btn-default">Pegawai</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/anak') }}"><button class="btn btn-outline btn-default">Anak</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/karya') }}"><button class="btn btn-outline btn-default">Karya Tulis</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/diklat') }}"><button class="btn btn-outline btn-default">Diklat</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pendidikan') }}"><button class="btn btn-outline btn-default">Pendidikan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_pangkat_golongan') }}"><button class="btn btn-outline btn-default">Pangkat Golongan</button></a>
            <a href="{{ url('pegawai/'.$pg->id_pegawai.'/riwayat_gaji_berkala') }}"><button class="btn btn-outline btn-default">Gaji Berkala</button></a>
        <?php 
            }
        ?>

    </div>
    </div>
</div>


	<form class="form-horizontal row" action="{{ url('pegawai/'.$pg->id_pegawai.'/anak/'.$anak->id_anak) }}" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Identitas Anak

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">

        	
			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Anak</label>
				<div class="col-sm-4">
				<input type="text" name="nama_anak" autocomplete="off" class="form-control text-uppercase" value="{{$anak->nama_anak}}" >
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Kelamin</label>
				<div class="col-sm-4">
				<div class="radio">
					<?php 
				$jk_l="";
				$jk_p="";
				if($anak->jenis_kelamin=="L"){
					$jk_l="checked";
				}else{
					$jk_p="checked";
				}
			?>
			  <label><input type="radio" name="jenis_kelamin" value="L" {{ $jk_l }}>Laki-laki</label>
			  <label><input type="radio" name="jenis_kelamin" value="P" {{ $jk_p }}>Perempuan</label>
				</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tempat Lahir</label>
				<div class="col-sm-3">
				<input type="text" name="tempat_lahir" class="form-control text-uppercase" autocomplete="off" value="{{$anak->tempat_lahir}}" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal SK</label>
				<div class="col-sm-1">
				<input type="text" name="tanggal_lahir" class="form-control text-uppercase" autocomplete="off" value="{{ $tanggal_lahir['d'] }}">
				</div>

				<?php 
				
				$Januari="";$Februari="";$Maret="";$April="";$Mei="";$Juni="";$Juli="";$Agustus="";$September="";$Oktober="";$November="";$Desember="";

				if($tanggal_lahir['m']=="01")
				{
					$Januari="selected";
				}
				else if($tanggal_lahir['m']=="02")
				{
					$Februari="selected";
				}
				else if($tanggal_lahir['m']=="03")
				{
					$Maret="selected";
				}
				else if($tanggal_lahir['m']=="04")
				{
					$April="selected";
				}
				else if($tanggal_lahir['m']=="05")
				{
					$Mei="selected";
				}
				else if($tanggal_lahir['m']=="06")
				{
					$Juni="selected";
				}
				else if($tanggal_lahir['m']=="07")
				{
					$Juli="selected";
				}
				else if($tanggal_lahir['m']=="08")
				{
					$Agustus="selected";
				}
				else if($tanggal_lahir['m']=="09")
				{
					$September="selected";
				}
				else if($tanggal_lahir['m']=="10")
				{
					$Oktober="selected";
				}
				else if($tanggal_lahir['m']=="11")
				{
					$November="selected";
				}
				else if($tanggal_lahir['m']=="12")
				{
					$Desember="selected";
				}

				?>
				<div class="col-sm-2">
				<select class="form-control" name="bulan_lahir">
					<optgroup label="Bulan">
					<option value="01" {{ $Januari }} >Januari</option>
					<option value="02" {{ $Februari }} >Februari</option>
					<option value="03" {{ $Maret }} >Maret</option>
					<option value="04" {{ $April }} >April</option>
					<option value="05" {{ $Mei }} >Mei</option>
					<option value="06" {{ $Juni }} >Juni</option>
					<option value="07" {{ $Juli }} >Juli</option>
					<option value="08" {{ $Agustus }} >Agustus</option>
					<option value="09" {{ $September }} >September</option>
					<option value="10" {{ $Oktober }} >Oktober</option>
					<option value="11" {{ $November }} >November</option>
					<option value="12" {{ $Desember }} >Desember</option>
					

					</optgroup>
				</select>
				</div>

				<div class="col-sm-2">
				<input type="text" class="form-control" name="tahun_lahir" autocomplete="off" value="{{ $tanggal_lahir['y']}}">
				</div>

			</div>
<br>
			<div class="form-group">
				<label class="col-sm-3 control-label">Anak Dari</label>
				<div class="col-sm-4">
				<input type="text" name="nama" autocomplete="off" class="form-control text-uppercase" value="{{$pg->nama}}" disabled>
				</div>
			</div>

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Status</label>
	          <div class="col-lg-3">
	          <?php 

	          	$kandung="";
	          	$asuh="";
	          	$angkat="";

	          	if($anak->status=="Anak kadung")
	          	{
	          		$kandung="selected";
	          	}
	          	else if($anak->status=="Anak asuh")
	          	{
	          		$asuh="selected";
	          	}
	          	else if($anak->status=="Anak angkat")
	          	{
	          		$angkat="selected";
	          	}

	          ?>

	            <select ui-jq="chosen" class="form-control" name="status">
	                <optgroup label="Pilih Status">
	                    <option value="Anak kandung" {{ $kandung }}>Anak Kandung</option>
	                    <option value="Anak asuh" {{ $asuh }}>Anak Asuh</option>
	                    <option value="Anak angkat" {{ $angkat }}>Anak Angkat</option>
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>
<br>

			<div class="form-group">
				<label class="col-sm-3 control-label">NISN</label>
				<div class="col-sm-4">
				<input type="text" name="NISN" autocomplete="off" class="form-control text-uppercase" value="{{$anak->NISN}}">
				</div>
			</div>

			<div class="form-group"><td>
	          <label class="col-sm-3 control-label">Jenjang</label>
	          <div class="col-lg-3">
	          <?php
	          	$sd="";
	          	$smp="";
	          	$sma="";

	          	if($anak->jenjang=="SD/sederajat")
	          	{
	          		$sd="selected";
	          	}
	          	else if($anak->jenjang=="SMP/sederajat")
	          	{
	          		$smp="selected";
	          	}
	          	else if($anak->jenjang=="SMA/sederajat")
	          	{
	          		$sma="selected";
	          	}

	          ?>

	            <select ui-jq="chosen" class="form-control" name="jenjang">
	                <optgroup label="Pilih Jenjang">
	                    <option value="SD/sederajat" {{ $sd }}>SD/sederajat</option>
	                    <option value="SMP/sederajat" {{ $smp }}>SMP/sederajat</option>
	                    <option value="SMA/sederajat"{{ $sma }}>SMA/sederajat</option>
	                    
	                </optgroup>
	            </select>
	          </div>
	         </td>
	        </div>

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
	<input type="hidden" name="_method" value="PUT">
	</form>


<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });
    $('#tanggal_lahir').on('change',function(){
    	var bod=$(this).val();
    	var myyear= bod.slice(0,4);
    	var year=new Date().getFullYear();
    	var age=year-myyear;

    	var pensiun= year+(60-age);
    	$('#tahun_pensiun').val(pensiun);
    	$('#masa_kerja').val(60-age);

    });

</script>  

<script>
	
	$(document).ready(function () {
   	$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    $('.expander-drop').slideUp(200);
	});	

</script>


@endsection
