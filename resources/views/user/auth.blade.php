@extends('layout/master')
@section('title','Halaman User')
@section('content')


<div class="row">

    <div class="col-lg-12">
    <div  class="page-header">

        <h4 class="text-uppercase">Halaman User</h4>	

    </div>
    </div>
</div>


	<form class="form-horizontal row" action="" method="post">

	<div class="row">
	<div class="col-lg-10">
    <div class="panel panel-default">
    	
        <div class="panel-heading">
        <div class="expander">
        	<i class="fa fa-angle-down pull-right"></i>

            Data User

        </div>
        </div>
		
		<div class="expander-drop-first">
        <div class="panel-body">
        <div class="row">



        	
			<div class="form-group">
				<label class="col-sm-3 control-label">Username</label>
				<div class="col-sm-3">
				<input type="text" name="username" autocomplete="off" class="form-control text-uppercase" value="{{$user->username}}" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Password Sekarang</label>
				<div class="col-sm-3">

				<input type="password" name="old_password" class="form-control text-uppercase" autocomplete="off" value="">
				
				</div>
			</div>
			<br>
			
			<input type="hidden" name="id_user" autocomplete="off" class="form-control text-uppercase"  value="{{$user->id_user}}">

			<div class="form-group">
				<label class="col-sm-3 control-label">Password Baru</label>
				<div class="col-sm-3">
				<input type="password" name="new_password" autocomplete="off" class="form-control text-uppercase" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Ketik Ulang Password Baru</label>
				<div class="col-sm-3">
				<input type="password" name="new_password_confirmation" class="form-control text-uppercase" autocomplete="off" value="">
				</div>
			</div>

			

			

		</div>
		</div>
		</div>

	</div>
	</div>
	</div>


		<div class="panel-body col-lg-10">
		
		<div class="form-group">

		<button type="submit" class="btn btn-success btn-block">Simpan</button>
		</div>
		</div>
		
		<br>
		<br>


		{{ csrf_field() }}
	<input type="hidden" name="_method" value="PUT">
	</form>





<script>
	
	$(document).ready(function () {
   	$('.expander-drop').slideDown(200);
    $('.expander').click(function () {
        // .parent() selects the A tag, .next() selects the P tag
        $(this).parent().next().slideToggle(200);
    });
    $('.expander-drop').slideUp(200);
	});	

</script>


@endsection
