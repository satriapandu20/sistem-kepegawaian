@extends ('layout/master')
@section ('title','Beranda')
@section ('content')
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Selamat Datang - {{ strtoupper($pg->nama) }}</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <table class="table table-striped table-bordered table-hover">
                    <thead>

                    <tr>
                        
                        <th class="col-lg-3">Tahun Pensiun</th>
                        <th class="col-lg-3">Kenaikan Gaji Berkala</th>

                    </tr>
                    </thead>

                    <tbody>

                    
                    <tr>
                        
                        <td class="text-uppercase">{{ $pg->tahun_pensiun}}</td>
                        <td class="text-uppercase">{{ $kgb }}</td>
                        
                    </tr>

                    

                    </tbody>

                </table>

                <p style="color:red;animation: blink 1s infinite;">{{ $pendidikan }}</p>
            </div>
            <!-- /.row -->

@endsection
