@extends ('layout/master')
@section ('title','Kenaikan Gaji Berkala')
@section ('content')

<div class="row">

    <div class="col-lg-12">
    <div class="page-header">

        <h3>Daftar Kenaikan Gaji Berkala 3 Bulan Akan Datang</h3>
        
     </div>
     </div>
</div>





<!-- <div class="col-xs-4 pull-right">
        	<input type="text" class="form-control" placeholder="Cari...">
            
       	</div> -->
<p>Kenaikan Gaji Berkala Pada - <b> {{ $m.' '.$y }} </b></p>
<p></p>
<br>
<br>   
	

	<table class="table table-striped table-bordered table-hover">
	<thead>

	<tr>
		<th>#</th>
		<th>Nama</th>
		<th class="col-lg-5">TMT Kenaikan Gaji Berkala Terakhir</th>
		<th align="center">Notifikasi</th>

	</tr>
	</thead>

	<tbody>

	@foreach($ngb as $ngb)
	<tr>
		<td></td>
		<td class="text-uppercase">{{$ngb->nama}}</td>
		<td class="text-uppercase">{{$ngb->TMT_KGB}}</td>
		<td align="center" class="text-uppercase">{{$ngb->status}}</td>

	</tr>

	@endforeach

	</tbody>

	</table>
	</div>
</div>
</div>

@endsection