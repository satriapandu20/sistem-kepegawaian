@extends ('layout/master')
@section ('title','Pensiun')
@section ('content')

<div class="row">

    <div class="col-lg-12">
    <div class="page-header">

        <h3>Daftar Pensiun - Tahun {{$tahun}}</h3>
        
     </div>
     </div>
</div>


<br>
<br>
<br>   
	

	<table class="table table-striped table-bordered table-hover">
	<thead>

	<tr>
		<th>#</th>
		<th>NIK</th>
		<th class="col-lg-5">Nama</th>
		<th align="center">Notifikasi</th>

	</tr>
	</thead>

	<tbody>

	@foreach($np as $np)
	<tr>
		<td></td>
		<td class="text-uppercase">{{$np->NIK}}</td>
		<td class="text-uppercase">{{$np->nama}}</td>
		<td align="center" class="text-uppercase">{{$np->status}}</td>

	</tr>

	@endforeach

	</tbody>

	</table>
	</div>
</div>
</div>

@endsection