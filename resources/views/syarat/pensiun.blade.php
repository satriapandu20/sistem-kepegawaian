@extends ('layout/master')
@section ('title','Persyaratan Kenaikan Pangkat Golongan')
@section ('content')
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Persyaratan Kenaikan Pangkat Golongan</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <ol>
                    <li>DPCP (Data Perorangan Calon penerima Pensiun)</li>
                    <li>Fotokopi SK Honor, CPNS, PNS, pangkat & jabatan terakhir berlegalisir</li>
                    <li>Fotokopi Kartu Pegawai berlegalisir</li>
                    <li>Fotokopi Surat kenaikan gaji berkala terakhir berlegalisir</li>
                    <li>Fotokopi Konversi NIP berligalisir</li>
                    <li>Daftar riwayat pekerjaan</li>
                    <li>Daftar susunan keluarga</li>
                    <li>Surat nikah/cerai berlegalisir</li>
                    <li>Surat lahir anak < 25 tahun, belum menikah, belum bekerja & masih sekolah</li>
                    <li>Surat keterangan masih sekolah untuk umur > 21 tahun</li>
                    <li>Surat Keterangan Kematian (Bagi pensiun janda/duda)</li>
                    <li>Surat hasil Tim Penguji Kesehatan yang telah ditunjuk oleh Pemerintah(Untuk Pensiun Keuzuran)</li>
                    <li>Pas photo PNS yang mengusulkan ukuran 4 x 6 = 7 lembar (untuk pensiun BUP dan APS); pas photo suami/istri yang meninggal dunia ukuran 4 x 6 = 7 lembar (pensiun janda/duda)</li>
                    <li>DP3 tahun terakhir atau PPK(Penilaian Prestasi Kerja)</li>
                    <li>Surat keterangan tidak pernah dijatuhi hukuman disiplin berat (Bermaterai dan ditandatangani oleh Kepala BKD atau Inspektur)</li>
                    <li>Surat Keterangan tidak pernah terlibat permasalahan hukum (Bermaterai dan ditandatangani oleh Kepala BKD atau Inspektur)</li>
                    
                </ol>
            </div>
            <!-- /.row -->

@endsection
